library(shiny)
library(ggplot2)
library(scales)
library(jsonlite)
library(grid)
library(gridBase)



json_data <- fromJSON("india.json");
Tweets1 = read.csv("AlleTwitterBerichten.csv", sep=";")
Tweets = read.csv("AlleTwitterBerichten.csv", sep=";")
Nieuwsdata = read.csv("NieuwsDataTwee.csv", sep =";")
Nieuwsdata$Date = as.Date(Nieuwsdata$Date)
Nieuwsdata$Rainfall = as.numeric(Nieuwsdata$Rainfall)


shinyServer(function(input, output) {

  output$welcome <- renderText({
   
  }
  )
  
  
  output$DateCountTweetsplot <- renderPlot({ 

    BD = input$Begindatum
    ED = input$Einddatum
    

    Tweets = as.Date(Tweets$date)
    DateCountTweets = aggregate(Tweets1$tags.text~Tweets, FUN=length)
    qplot(DateCountTweets$Tweets,DateCountTweets$`Tweets1$tags.text`,
          color=DateCountTweets$`Tweets1$tags.text`, 
          main = "Twittermessages about flooding in the monsoon season", 
          xlab="Date", 
          ylab ="Amount of tweets")+
          geom_line(size=1)+
          theme(axis.text.x=element_text(angle=70, hjust = 1, size = 12))+  
          scale_x_date(breaks = date_breaks("month"), limits = c(BD, ED))
    })
  
  output$Rainfall <- renderPlot({ 
    
    BDDD = input$Begindatummm
    EDDD = input$Einddatummm
    
    qplot(Nieuwsdata$Date,Nieuwsdata$Rainfall, 
          geom = "line", 
          main = "Rainfall over the years that caused floodings", 
          xlab = "Date", 
          ylab = "Rainfall Density") + 
          geom_line(aes(colour = Nieuwsdata$Rainfall),size = 1.3) +  theme(axis.text.x=element_text(angle=70, hjust = 1, size = 12))+ 
      scale_x_date(breaks = date_breaks("month"), limits = c(BDDD, EDDD))
  })
  
  output$RainfallMon <- renderPlot({ 
    Tweets = as.Date(Tweets$date)
    DateCountTweets = aggregate(Tweets1$tags.text~Tweets, FUN=length)
    
    BDD = input$Begindatumm
    EDD = input$Einddatumm
    
    plot.new()
    gl <- grid.layout(2,1)
    vp.1 <- viewport(layout.pos.row = 1, layout.pos.col = 1)
    vp.2 <- viewport(layout.pos.row = 2, layout.pos.col = 1)
    pushViewport(viewport(layout=gl))
    
    pushViewport(vp.1)
    p = qplot(Nieuwsdata$Date,Nieuwsdata$Rainfall, 
          geom = "line", 
          main = "Rainfall over the years that caused floodings", 
          xlab = "Date", 
          ylab = "Rainfall Density") + 
          geom_line(aes(colour = Nieuwsdata$Rainfall),size = 1.3)  +  theme(axis.text.x=element_text(angle=70, hjust = 1, size = 12))+ 
      scale_x_date(breaks = date_breaks("month"), limits = c(BDD, EDD))
    print(p, newpage = FALSE)
    popViewport()
    
    pushViewport(vp.2)

    l =     qplot(DateCountTweets$Tweets,DateCountTweets$`Tweets1$tags.text`,
                  color=DateCountTweets$`Tweets1$tags.text`, 
                  main = "Twittermessages about flooding in the monsoon season", 
                  xlab="Date", 
                  ylab ="Amount of tweets")+
      geom_line(size=1)+
      theme(axis.text.x=element_text(angle=70, hjust = 1, size = 12))+  
      scale_x_date(breaks = date_breaks("month"), limits = c(BDD, EDD))
    
    print(l, newpage = FALSE)
    popViewport(1)
  }) 
  
  output$NewsarticlePlots <- renderPlot({
    options(expressions=10000)
    
    BuD = input$Begindatuum
    EuD = input$Einddatuum
    
    
    plot.new()
    gl <- grid.layout(3,1)
    vp.1 <- viewport(layout.pos.row = 1, layout.pos.col = 1)
    vp.2 <- viewport(layout.pos.row = 2, layout.pos.col = 1)
    vp.5 <- viewport(layout.pos.row = 3, layout.pos.col = 1)
    pushViewport(viewport(layout=gl))
    
    
    
    Tweets = read.csv("AlleTwitterBerichten.csv", sep=";")
    Tweets = as.Date(Tweets$date)
    DateCountTweets = aggregate(Tweets1$tags.text~Tweets, FUN=length)
    
    pushViewport(vp.1)
    r = qplot(DateCountTweets$Tweets,DateCountTweets$`Tweets1$tags.text`,
              color=DateCountTweets$`Tweets1$tags.text`, 
              main = "Twittermessages about flooding in the monsoon season", 
              xlab="Date", 
              ylab ="Amount of Tweets")+geom_line(size=1)+  theme(axis.text.x=element_text(angle=70, hjust = 1, size = 12))+ 
      scale_x_date(breaks = date_breaks("month"), limits = c(BuD, EuD))
    print(r,newpage=FALSE)
    popViewport()
    
    
    Nieuwsdata = read.csv("nieuwsberichtenDAta.csv", sep =",")

    Nieuwsdata$Date = as.Date(Nieuwsdata$Date)
    Nieuwsdata$Rainfall = as.integer(Nieuwsdata$Rainfall)
    attach(Nieuwsdata)
    

    
    pushViewport(vp.2)
    q = qplot(Nieuwsdata$Date,Nieuwsdata$Rainfall, 
              geom = "line", 
              main = "Rainfall over the years that caused floodings", 
              xlab = "Date", ylab = "Rain (mm)") + geom_line(aes(colour = Rainfall),size = 1.3)+  theme(axis.text.x=element_text(angle=70, hjust = 1, size = 12))+ 
      scale_x_date(breaks = date_breaks("month"), limits = c(BuD, EuD))
    print(q,newpage=FALSE)
    popViewport(1)
    
    

    pushViewport(vp.5)
      gll = grid.layout(3,2)
      print(gll,newpage=FALSE)

    popViewport(2)
 
    pushViewport(viewport(layout=gll))
    vp.3 <- viewport(layout.pos.row = 3, layout.pos.col = 1)
    vp.4 <- viewport(layout.pos.row = 3, layout.pos.col = 2)
    

    pushViewport(vp.3)
    w = qplot(Nieuwsdata$Date,Nieuwsdata$People.affected, 
          geom = "line", 
          main = "People affected by floodings", 
          xlab = "Date", ylab = "Affected People") + geom_line(size = 1.3)+  theme(axis.text.x=element_text(angle=70, hjust = 1, size = 12))+ 
      scale_x_date(breaks = date_breaks("month"), limits = c(BuD, EuD))

    print(w,newpage=FALSE)
    popViewport()

    
    pushViewport(vp.4)
    e = qplot(Nieuwsdata$Date,Nieuwsdata$Died, 
          geom = "line", 
          main = "People died by floodings", 
          xlab = "Date", ylab = "Amount of victims") + geom_line(size = 1.3)+  theme(axis.text.x=element_text(angle=70, hjust = 1, size = 12))+ 
      scale_x_date(breaks = date_breaks("month"), limits = c(BuD, EuD))
    
    print(e,newpage=FALSE)
    popViewport(1)

    
    detach(Nieuwsdata)

  })
  
  output$PopulationTweets <- renderPlot({
    TweetsMetSteden = read.csv("allTweetsCountedPop.csv", header=T)
    df = data.frame(TweetsMetSteden$city,TweetsMetSteden$count,TweetsMetSteden$pop)
    TweetsMetSteden = transform(TweetsMetSteden, Mean = as.integer(TweetsMetSteden$count) / as.integer(TweetsMetSteden$pop))
    SampleSteden = TweetsMetSteden[sample(1:nrow(TweetsMetSteden),20,replace = F),]
    par(mfrow=c(2,1))
    plot(as.integer(SampleSteden$pop), main = "Populations by city", ylab="Population")
    plot(SampleSteden$Mean, main = "Average tweets by city", ylab = "Average Tweets")
  
      
    
  })
  
  output$Tweetsandnews <- renderPlot({
    json_data <- fromJSON("allTweets.json");
    news_data = fromJSON("news-array.json")
    
    dates = as.Date(strptime(json_data$date, "%Y-%m-%d")) 
    news_dates = substr(news_data$Date, 1, 24) 
    news_dates = as.Date(strptime(news_dates, "%a %b %d %Y %H:%M:%S")) 
    
    news_dates = subset(news_dates, news_dates > as.Date("2014-07-25") & news_dates < as.Date("2014-10-15"))
    news_dates = unique(news_dates) 
    
    frequencies = json_data$city
    dateCountTweets = aggregate(frequencies~dates, FUN=length)
    news_dates_frame = as.data.frame(news_dates)
    
    ggplot(data = dateCountTweets, aes(x = dates, y = frequencies, color=frequencies)) +
      geom_line() +
      geom_point(size=3) +
      labs(
        color = "Amount of tweets",
        x = "Date",
        y = "Frequency",
        title = "Amount of tweets about floods in 2014"
      ) +
      geom_vline(data = news_dates_frame, aes(xintercept=as.numeric(news_dates))) +
      scale_x_date()

  })

  output$Rain <- renderPlot({
    rain_tot = read.csv("rain_tot.csv")
    ggplot(rain_tot,aes(as.Date(date),rain,group=1)) + 
      geom_point() + 
      theme(axis.text.x=element_text(angle=70, hjust = 1, size = 12)) +
      xlab("Date") + 
      ylab("Amount of rain (mm)") + 
      ggtitle("Amount of rain for the whole of India")
  })
  
  output$Elevation <- renderPlot({
    allTweetsCounted = read.csv("allTweetsCountedPlott.csv")
    qplot(factor(popcat), 
          data=allTweetsCounted, 
          geom="bar", fill=factor(popcat),
          xlab = "Population category",
          ylab="Amount of Cities",main="Amount of cities per Population Category")
  })
  
  output$Elevationn <- renderPlot({
    allTweetsCounted = read.csv("allTweetsCountedPlott.csv")
    qplot(factor(risk), data=allTweetsCounted, geom="bar", fill=factor(popcat),xlab = "Risk",ylab="Amount of Cities",main = "Amount of cities per Risk category")
  })
  
  output$Elevationnn <- renderPlot({
    allTweetsCounted = read.csv("allTweetsCountedPlott.csv")
    qplot(factor(riskbig), data=allTweetsCounted, geom="bar", fill=factor(popcat),xlab = "Risk",ylab="Amount of Cities",main = "Amount of cities per Risk (+0.05) category")
  })
  
  output$Elevationnnn <- renderPlot({
    allTweetsCounted = read.csv("allTweetsCountedPlott.csv")
    ggplot(allTweetsCounted,aes(popcat,fill=popcat)) + geom_histogram(binwidth=0.5) + xlab("Population category") + ylab("Amount of Cities") + ggtitle("Amount of cities per Population Category")
  })
  output$Elevationnnnn <- renderPlot({
    allTweetsCounted = read.csv("allTweetsCountedPlott.csv")
    ggplot(allTweetsCounted,aes(risk,fill=popcat)) + geom_histogram(binwidth=0.5) + scale_x_continuous(breaks=c(0,1),labels=c("NoRisk","Risk"),limits=c(0,1)) + xlab("Risk") + ylab("Amount of Cities") + ggtitle("Amount of cities per Risk category")
  })
  output$Elevationnnnnn <- renderPlot({
    allTweetsCounted = read.csv("allTweetsCountedPlott.csv")
    ggplot(allTweetsCounted,aes(riskbig,fill=popcat)) + geom_histogram(binwidth=0.5) + scale_x_continuous(breaks=c(0,1),labels=c("NoRisk","Risk"),limits=c(0,1)) + xlab("Risk") + ylab("Amount of Cities") + ggtitle("Amount of cities per Risk (+0.05) category")
  })
  output$Elevationnnnnnn <- renderPlot({
    allTweetsCounted = read.csv("allTweetsCountedPlott.csv")
    ggplot(allTweetsCounted,aes(risk,fill=popcat)) + geom_histogram(binwidth=0.5,position = "dodge") + scale_x_discrete(breaks=c(0,1),labels=c("NoRisk","Risk"),limits=c(0,1)) + xlab("Risk") + ylab("Amount of Cities") + ggtitle("Amount of cities per Risk category")
  })
  output$Elevationnnnnnnn <- renderPlot({
    allTweetsCounted = read.csv("allTweetsCountedPlott.csv")
    ggplot(allTweetsCounted,aes(riskbig,fill=popcat)) + geom_histogram(binwidth=0.5,position = "dodge") + scale_x_continuous(breaks=c(0,1),labels=c("NoRisk","Risk"),limits=c(0,1)) + xlab("Risk") + ylab("Amount of Cities") + ggtitle("Amount of cities per Risk (+0.05) category")
  })
  
  
  
  output$EElevation <- renderPlot({  
  allTweetsCounted = read.csv("allTweetsCountedPlott.csv")
  ggplot(allTweetsCounted,aes(riskbig,count,fill=popcat)) + geom_bar(stat="identity",position = "dodge") + scale_x_continuous(breaks=c(0,1),labels=c("NoRisk","Risk"),limits=c(0,1)) + xlab("Risk") + ylab("Amount of Tweets") + ggtitle("Amount of cities per Risk (+0.05) category") + theme(legend.position = c(0.8, 0.5)) + labs(fill = "Population Categorie")
  })
  
  output$EElevationn <- renderPlot({
    allTweetsCountedNoRiskBigSmall = read.csv("allTweetsCountedNoRiskBigSmall.csv")
    ggplot(allTweetsCountedNoRiskBigSmall,aes(min,countpop)) + geom_point() + geom_smooth() + xlab("Distance (km)") + ylab("Amount of Tweets per inhabitant")
  })
  
  output$km <- renderPlot({
    allTweetsCountedNoRiskBigSmall = read.csv("allTweetsCountedNoRiskBigSmall.csv")
    allTweetsCountedNoRiskBigSmall$km = (allTweetsCountedNoRiskBigSmall$min/0.1)*11.132
    ggplot(allTweetsCountedNoRiskBigSmall,aes(km,countpop)) + geom_point() + geom_smooth() + 
    xlab("Distance (km)") + 
    ylab("Amount of Tweets per inhabitant") +
    ggtitle("Distance from Riskarea in km correlated with amount of Tweets")
  })
  
  
  output$Rainn <- renderPlot({
    rainfallaggr = read.csv("rainfallaggr.csv")
    ggplot(rainfallaggr,aes(as.Date(date),mean,group=1)) + 
      geom_point() + 
      theme(axis.text.x=element_text(angle=70, hjust = 1, size = 12)) + 
      geom_smooth() + geom_line() +
      xlab("Date") +
      ylab("Mean Rain in mm") +
      ggtitle("Mean rain on a specific day for the whole of India")
  })
  
  output$elevationrain <- renderPlot({
    lotsofrainn = read.csv("rainforplotss.csv")
    risktrue = subset(lotsofrainn,lotsofrainn$risk == 1)
    riskfalse = subset(lotsofrainn,lotsofrainn$risk == 0)
    risktruebig = subset(lotsofrainn,lotsofrainn$riskbig == 1)
    riskfalsebig = subset(lotsofrainn,lotsofrainn$riskbig == 0)
    
    
    
    meanfalse = aggregate(riskfalse$rain~riskfalse$date,FUN=mean)
    meantrue = aggregate(risktrue$rain~risktrue$date,FUN=mean)
    colnames(meanfalse) = c("date","rain")
    colnames(meantrue) = c("date","rain")
    meanfalse$date = as.Date(meanfalse$date)
    meantrue$date = as.Date(meantrue$date)
    
    ggplot()  + geom_line(data=meanfalse,aes(date,rain),color="red") + geom_line(data=meantrue,aes(date,rain),color="blue") + 
    ylab("rain (mm)") +
    ggtitle("Rain in no risk areas and risk areas")
  })
  
  output$Correlation <- renderPlot ({
    BDC = input$BegindatumC
    EDC = input$EinddatumC
    allTweetsCounted = read.csv("Tweetsforrainn.csv")
    allTweetsCounted$date = as.Date(allTweetsCounted$date)
    allTweetsCounted$counttt = (allTweetsCounted$pop/ allTweetsCounted$count) / 100000
    aggrdaterain = aggregate(allTweetsCounted$rain~allTweetsCounted$date,FUN=mean)
    aggrdatecount = aggregate(allTweetsCounted$counttt~allTweetsCounted$date,FUN=mean)
    colnames(aggrdatecount) = c("date","count")
    colnames(aggrdaterain) = c("date","rain")
    
    
    aggr = merge(aggrdatecount,aggrdaterain)
    
    ggplot() + geom_point(data=aggr,aes(date,count),color="red") + geom_point(data=aggr,aes(date,rain),color="blue") + geom_smooth(data=aggr,aes(date,count),color="red") + geom_smooth(data=aggr,aes(date,rain),color="blue") +
      scale_x_date(breaks = date_breaks("month"), limits = c(BDC, EDC)) +
      ggtitle("Correlation between rain and amount of Tweets")
    
  })
  
  output$elevationrainn <- renderPlot({
    lotsofrainn = read.csv("rainforplotss.csv")
    risktrue = subset(lotsofrainn,lotsofrainn$risk == 1)
    riskfalse = subset(lotsofrainn,lotsofrainn$risk == 0)
    risktruebig = subset(lotsofrainn,lotsofrainn$riskbig == 1)
    riskfalsebig = subset(lotsofrainn,lotsofrainn$riskbig == 0)
    
    
    meanfalsebig = aggregate(riskfalsebig$rain~riskfalsebig$date,FUN=mean)
    meantruebig = aggregate(risktruebig$rain~risktruebig$date,FUN=mean)
    colnames(meanfalsebig) = c("date","rain")
    colnames(meantruebig) = c("date","rain")
    meanfalsebig$date = as.Date(meanfalsebig$date)
    meantruebig$date = as.Date(meantruebig$date)
    
    ggplot()  + geom_line(data=meanfalsebig,aes(date,rain),color="red") + geom_line(data=meantruebig,aes(date,rain),color="blue")  + 
      ylab("rain (mm)") +
      ggtitle("Rain in no risk areas and risk areas")
    
    })
  
  output$Tweetsnewsrain <- renderPlot({
    json_data <- fromJSON("allTweets.json")
    news_data = fromJSON("news-array.json")
    rain_data = read.csv("rain_tot.csv")
    
    dates = as.Date(strptime(json_data$date, "%Y-%m-%d")) 
    news_dates = subset(news_data, select=c("Date", "Rainfall"))
    news_dates$Date = substr(news_dates$Date, 1, 24) 
    news_dates$Date = as.Date(strptime(news_dates$Date, "%a %b %d %Y %H:%M:%S")) 
  
    news_dates = subset(news_dates, Date > as.Date("2014-07-25") & Date < as.Date("2014-10-15"))
 
    
    frequencies = json_data$city
    DateCountTweets = aggregate(frequencies~dates, FUN=length)

    
    rainfall = aggregate(rain_data$rain~rain_data$date, FUN=sum)
    colnames(rainfall) = c("date", "rainfall")
    rainfall$date = as.Date(rainfall$date)
    
    ggplot() +
      geom_line(data = DateCountTweets, aes(x = dates, y = frequencies, color="# of Tweets")) +
      geom_line(data = rainfall, aes(x = date, y = rainfall, colour="rainfall")) +
      geom_point(size=3) +
      labs(
        color = "Amount of tweets",
        x = "Date",
        y = "Frequency",
        title = "Amount of tweets about floods in 2014"
      ) +
      geom_vline(data = news_dates, aes(xintercept=as.numeric(Date))) +
 
      scale_color_manual(values=c("rainfall"="blue", "# of Tweets"="red")) +
      scale_x_date()
 
  })
  
  output$Tweetspercity <- renderPlot({ 
    BDT = input$BegindatumT
    EDT = input$EinddatumT
    
    stadT = input$checkGroupT 
   
    allTweetsCounted = read.csv("Tweetsforrainn.csv")

    allTweetsCounted$date = as.Date(allTweetsCounted$date)
    tweets = subset(allTweetsCounted,city==stadT)
    allTweetsCounted$date = as.Date(allTweetsCounted$date)

    ggplot() +  
      geom_line(data=tweets, aes(date, countt), colour = "red") + 
      xlab("Date") + 
      ylab("Tweet Count") +
      ggtitle("Amount of tweets per city") +
      scale_x_date(breaks = date_breaks("month"), limits = c(BDT, EDT)) 
  })
  
  output$RainTweets <- renderPlot({ 
    BDR = input$BegindatumR
    EDR = input$EinddatumR
    
    stadR = input$checkGroupR
    
    lotsofrainn = read.csv("rainforplotss.csv")
    allTweetsCounted = read.csv("Tweetsforrainn.csv")
    lotsofrainn$date = as.Date(lotsofrainn$date)
    allTweetsCounted$date = as.Date(allTweetsCounted$date)
    tweets = subset(allTweetsCounted,city==stadR)
    allTweetsCounted$date = as.Date(allTweetsCounted$date)
    rain = subset(lotsofrainn,city == stadR)
    rain$date = as.Date(rain$date)
    ggplot() + 
      geom_line(data=rain, aes(date, rainn), colour = "blue") + 
      geom_line(data=tweets, aes(date, countt), colour = "red") + 
      xlab("Date") + 
      ylab("Rain (mm)/ Tweet Count") +
      ggtitle("Amount of rain and Tweets per city") +
      scale_x_date(breaks = date_breaks("month"), limits = c(BDR, EDR)) 
  })
  
  output$Rainpercity <- renderPlot({ 
    BBD = input$BBegindatum
    EED = input$EEinddatum
    
    stad = input$checkGroup 
    lotsofrainn = read.csv("rainforplotss.csv")
    
    lotsofrainn$date = as.Date(lotsofrainn$date)
    
    
    rain = subset(lotsofrainn,city == stad)
    rain$date = as.Date(rain$date)
    ggplot() + 
      geom_line(data=rain, aes(date, rainn), colour = "blue") + 
      xlab("Date") + 
      ylab("Rain (mm)") +
      ggtitle("Amount of rain per city") +
      scale_x_date(breaks = date_breaks("month"), limits = c(BBD, EED)) 
  })
  
  
  output$SourceTwee <- renderPlot({ 
    tweets = read.csv("allTweetsWithSources.csv")
    
    tweets = read.csv("allTweetsWithSources.csv")
    
    pattern = '<a href=.+>(.+)</a>';
    spacesPattern = '/ +(?= )/g';
    
    tweets = cbind(tweets, gsub(pattern, "\\1", tweets$source))
    colnames(tweets)[length(names(tweets))] <- "ProcessedSource"
    tweets$ProcessedSource = gsub("(?<=[\\s])\\s*|^\\s+$", "", tweets$ProcessedSource, perl=TRUE)
    
    sources = table(tweets$ProcessedSource)
    sources = as.data.frame(sources)
    names(sources)[1] = 'source'
    
    sources = subset(sources, Freq > 100)
    sources = sources[-c(41), ]
    
    ggplot(data=sources, aes(x=source, y = Freq)) +
      geom_bar(stat="identity") +
      ggtitle("Tweet sources") +
      xlab("Source") +
      theme(axis.text.x = element_text(angle = 70, hjust = 1, size = 12))
    

  })
  output$SourceDrie <- renderPlot({ 
    tweets = read.csv("allTweetsWithSources.csv")
    
    tweets = read.csv("allTweetsWithSources.csv")
    
    pattern = '<a href=.+>(.+)</a>';
    spacesPattern = '/ +(?= )/g';
    
    tweets = cbind(tweets, gsub(pattern, "\\1", tweets$source))
    colnames(tweets)[length(names(tweets))] <- "ProcessedSource"
    tweets$ProcessedSource = gsub("(?<=[\\s])\\s*|^\\s+$", "", tweets$ProcessedSource, perl=TRUE)
    
    sources = table(tweets$ProcessedSource)
    sources = as.data.frame(sources)
    names(sources)[1] = 'source'
    
    sources = subset(sources, Freq > 100)
    sources = sources[-c(41), ]
    
    ggplot(data=sources, aes(x=source, y = Freq)) +
      geom_bar(stat="identity") +
      ggtitle("Tweet sources") +
      xlab("Source") +
      theme(axis.text.x = element_text(angle = 70, hjust = 1, size = 12)) +
      scale_y_log10()
    
    
  })
  
  output$Created <- renderPlot({ 
    json_data$created_at = strptime(json_data$created_at, "%a %b %d %H")
    
    ggplot(data=json_data, aes(x=created_at, fill=metadata$iso_language_code)) + 
      geom_bar() +
      ggtitle("# of new tweets per hour per ISO language") +
      xlab("# of tweets per hour") + 
      scale_fill_discrete(guide = guide_legend(title = "ISO language code"))
  })
  
  output$Source <- renderPlot({ 
    json_source_data = json_data
    
    pattern = '<a href=".+">(.+)</a>';
    
    json_source_data = cbind(json_source_data, gsub(pattern, "\\1", json_source_data$source))
    colnames(json_source_data)[length(names(json_source_data))] <- "ProcessedSource"
    json_source_data$ProcessedSource = gsub("(?<=[\\s])\\s*|^\\s+$", "", json_source_data$ProcessedSource, perl=TRUE)
    
    ggplot(data=json_source_data, aes(x=ProcessedSource)) +
      geom_bar() +
      ggtitle("Tweet sources") +
      xlab("Source") +
      theme(axis.text.x = element_text(angle = 70, hjust = 1, size = 12))
  })

  
  output$Country <- renderPlot({ 
    ggplot(data=json_data, aes(x=place$country)) +
      geom_bar(stat="count") +
      ggtitle("# of users per country") +
      xlab("Country") 
      
  })
  
  output$Timezone <- renderPlot({ 
    ggplot(data=json_data, aes(x=user$time_zone)) +
      geom_bar() +
      ggtitle("# of users per timezone") +
      xlab("Timezone") +
      theme(axis.text.x = element_text(angle = 70, hjust = 1, size = 12))
  })
  
  output$Language <- renderPlot({ 
    ggplot(data=json_data, aes(x=user$lang)) +
      geom_bar() +
      ggtitle("# of users per language") +
      xlab("Language")
  })
  
  output$Isocode <- renderPlot({ 
    ggplot(data=json_data, aes(x=metadata$iso_language_code, fill=metadata$iso_language_code)) + 
      geom_bar() +
      ggtitle("Distribution of the written language of tweets") +
      xlab("ISO language code") + 
      scale_fill_discrete(guide = guide_legend(title = "ISO language code"))
  })
  
})