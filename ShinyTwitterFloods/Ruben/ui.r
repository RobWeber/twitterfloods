library(shiny)
shinyUI(navbarPage
  ("Floodflamingos",
  tabPanel("Welcome",
           h1('Welcome!'), 
           p('On this website you can look at all the plots made by the Floodflamingos.'),
           p('For the minor Big Open Data we were assigned the task to come up with a big data idea which would incorporate all the topics taught to us during our classes. We were free to either come up with our own idea, or pick one of many idea pitched to us during our first meeting. After Abbey pitched her idea, we decided to help the cause and join FloodTags in their endeavor to help people in areas that are prone to flood.)'),
           p('FloodTags is a social enterprise established right after the disasters of hurricane Catharinae. Floodtags believed that people could be warned before floods happen and rescue services could be guided by using the power of social media. They believed that one could use Tweets to accomplish this task. Now, years later, FloodTags has developed from an ambitious idea into an actual enterprise, partnering up with big names and saving many lives during the process.'),
           p('Our ambition for FloodTags was to increase their radius. As they are now operating mostly on Indonesia. We considered several countries and continents as our target but we finally settled on India. India being the number 1 country with floods, we aspired to help the people of India. Almost every year around the monsoon period there are floods in India. Monsoon is traditionally defined as a seasonal reversing accompanied by corresponding changes in , but is now used to describe seasonal changes in atmospheric circulation and precipitation associated with the asymmetric heating of land and sea(Wikipedia,2015).'),
           p('During our project we came up with this research question:'),
           p(strong('Can floods in India be predicted through the use of Tweets, weather data, elevation data and news articles?')),
           br(),
           p('During our 20 weeks long research we did our best to answer this hypothesis. Our results are in this paper. To determine if it is possible to use twitter with other resources to predict floods, we asked ourself the following research questions:'),
           p(strong('- Is there a correlation between tweets and rainfall?')),
           p(strong('- Is there a correlation between tweets and the news(and the data in in the news, like affected people by floods)')),
           p(strong('- How accurate are the tweets by city if we compared it with the news?')),
           p(strong('- Is there correlation between tweets and living close to a mountain?')),
           p(strong('- Is there sufficient correlation between tweets and rainfall on city level?')),
           img(src="flamingo.gif"),
           img(src="india.png",height=291,width=280)
           ),
  
  navbarMenu("Plots from News Articles",  
  tabPanel("News Articles",
     sidebarLayout(
       sidebarPanel(
         dateInput("Begindatuum", "Start Date:", value = "2013-06-01"),
         dateInput("Einddatuum", "End Date:", value = "2015-09-30"), width=2
       ),                     
      mainPanel(
        plotOutput("NewsarticlePlots",height=900,width=1100),
        p('If we first take a look at the plot with the twitter messages and the rainfall that causes floods, you can see they have the same trend lines. In 2013 there was one period in India where heavy rainfall caused floods, so in that event there was also one peak of twitter messages about the floods. In 2014 there was a longer period of rainfall and that was visible in the twitter messages. However there was less rainfall that causes floods than in 2013, but there were more twitter messages about floods in 2014.'),
        p('The floods in 2014 that occurred in Jammu and Kashmir affected more people and there were  more twitter messages, so there is also a correlation between the twitter messages and the affected people. Strangely enough, in our news data we found that in 2014 there were more affected people by floods, but less victims actually died from the floods. In 2013 there were less affected people, but a lot of victims. So we have doubts about our accuracy of the news data.'),
        p('In 2015 there were was a lot of rainfall periods in India during the monsoon. You can see in the plot of rainfall that there are four peaks of heavy rainfall in 2015, those peaks you can also find in the twitter messages during that period. Strange enough there are not a lot of twitter messages, but there was more rainfall that caused floods and more people were affected by the floods.'),
        p('In general, we can see in the plots there is a correlation between twitter messages and data from the news. Only it is not a really strong correlation, if there is a peak of affected people or rainfall, then there is a peak in twitter messages, but the tweets don’t respond on the amount of rainfall or affected people. So if there are more people affected by floods than last year, does not necessarily mean there are more tweets about it.')
     )
     )
    ),
    tabPanel("Tweets and News",
             mainPanel(
               p(strong('When a flood occurs, there is a substantial spike in the number of Tweets about floods')),
               plotOutput("Tweetsandnews"),
               p('To answer our first hypothesis, we took a sample of our Tweets dataset. We counted the amount of Tweets per day for 2014 and plotted the frequencies against the date. Following up, we combined this data with news articles reporting about floods. If a spike matches an article, we assume the spike in Tweets is indeed due to a flood.'),
               p('The amount of news articles we have isn’t substantial. The Tweet sample is from around the monsoon season from (2014)  in which it is very likely that floods happen. The vertical lines represent the publishing of a news article about a flood occurring. There does seem to be a relationship between articles and the increase of Tweets about floods. We can say the hypothesis is true.')
               
             )
    )
  ),

  
  navbarMenu("Rain (Whole of India)",
     tabPanel("Only Rain",
              mainPanel(
                p('The following plots are made with the extracted data from the package raincpc. It uses the amount of rain, the date and a specific lat and lon combination (with steps of 0.5 lat and 0.5 lon).'),
                p('The rainfall data extracted is of the whole of India. Of course this is a lot of data, but we also questioned if it would actually make useful plots. Mainly because India is a very big country and there are a lot of differences in climate between the norther part of India and the southern part. Still we decided to make these plot and show them on Shiny, just to see some trends in the rainfall data of the whole of India. Maybe when there are more tweets available in a greater time period it will be handy to lay them next to the rain data of the whole of India. For now it was very difficult to check for these three months if there was any correlation between the rainfall in the whole of India, the rainfall of a specific city and the corresponding tweets with all those variables.'),
                plotOutput("Rain"),
                p('This plot shows all the specific data points, where on the x-axis the date is shown and on the y-axis we can see the amount of rain. Because India is very big we can see that there are a lot of registered rain data points on a specific day. This makes the plot incredibly confusing. Still we can make out the outliers like we can see on the top right side. It is possible that these group of points have very similar longitude and latitude values, which means that they are rather close together. After this we can check why there is a lot of rain there and if villages are nearby. These villages can possibly be more prone to flooding.'),
                plotOutput("Rainn"),
                p('This graph shows the mean rain of a specific day, so there is only one datapoint per day left. With this data we can analyse the trend of the amount of rain through a year or through a specific month. We only extracted the rain data from the time period we had tweets of. As stated before, this plot is not that useful because of the time period of the tweets we got from Floodtags. Still, we can point out the peaks and the outliers in the data and lay these next to the news articles about floods.'),
                p('The line in the middle shows the linear regression of the rain through the chosen time period. Our time period is around 3 months. This line is is a lot less interesting, because it flattens out the outlier. Still if you have a very large time period, you can for example analyse if in a specific month of a specific year there was more rain than in the same month of of the previous year.'),
                p(strong('Conclusions:')),
                p('These plots are very useful for some support information about the rain and the trends that occur in India. But actual prediction on floods of these plots is impossible. India as a whole has too much different climates and elevation which means that for every city other variables will cause the floods. Therefor using these plots for seeing trends in rain in a specific time period is fine, but to limit false conclusions, no more than that analysis is advised.')
              )
     ),
     tabPanel("Rain plus newsarticle",
              mainPanel(
                p(strong('When heavy rainfall occurs, somewhere in the following days a flood will occur in that general location')),
                p('We answered this hypothesis by combining the # of Tweets during the monsoon season with historical weather data of said monsoon season. The data is slightly noisy, but the data matches up. There is a clear pattern visible between rainfall and # of Tweets about flooding.'),
                plotOutput("Tweetsnewsrain"),
                p('Although not consistently, there are spikes of tweets after a spike in rainfall. It’s hard to say how much the increase in tweets means for each spike, but there is a clear relation between these two variables.')
              )
     )
  ),
  
  navbarMenu("Per city",

   tabPanel("Tweets",
            sidebarLayout(
              sidebarPanel(
                dateInput("BegindatumT", "Start Date:", value = "2014-07-23"),
                dateInput("EinddatumT", "End Date:", value = "2014-10-09"),
                selectInput("checkGroupT", label = h3("CityT"), 
                            choices = list("Abohar"="Abohar","Achalpur"="Achalpur","Adilabad"="Adilabad","Agartala"="Agartala","Agra"="Agra","Ahmedabad"="Ahmedabad","Ahmednagar"="Ahmednagar","Ajmer"="Ajmer","Aligarh"="Aligarh","Allahabad"="Allahabad","Alwar"="Alwar","Ambala"="Ambala","Ambernath"="Ambernath","Amravati"="Amravati","Amreli"="Amreli","Amritsar"="Amritsar","Anantapur"="Anantapur","Anantnag"="Anantnag","Arrah"="Arrah","Asansol"="Asansol","assam"="assam","Aurangabad"="Aurangabad","Badlapur"="Badlapur","Bahadurgarh"="Bahadurgarh","Bally"="Bally","Bangalore"="Bangalore","Barasat"="Barasat","Bardhaman"="Bardhaman","Bareilly"="Bareilly","Barnala"="Barnala","Batala"="Batala","Bathinda"="Bathinda","Begusarai"="Begusarai","Belgaum"="Belgaum","Bellary"="Bellary","Bhagalpur"="Bhagalpur","Bharatpur"="Bharatpur","Bhatpara"="Bhatpara","Bhavnagar"="Bhavnagar","Bhilai"="Bhilai","Bhilwara"="Bhilwara","Bhiwandi"="Bhiwandi","Bhiwani"="Bhiwani","Bhopal"="Bhopal","Bhubaneswar"="Bhubaneswar","Bhusawal"="Bhusawal","Bihar"="Bihar","Bijapur"="Bijapur","Bikaner"="Bikaner","Bilaspur"="Bilaspur","Bokaro"="Bokaro","Brahmapur"="Brahmapur","Bulandshahr"="Bulandshahr","Chandel"="Chandel","Chandigarh"="Chandigarh","Chandrapur"="Chandrapur","Chennai"="Chennai","Coimbatore"="Coimbatore","Cuttack"="Cuttack","Darbhanga"="Darbhanga","Dehradun"="Dehradun","Delhi"="Delhi","Dewas"="Dewas","Dhanbad"="Dhanbad","Dhubri"="Dhubri","Durg"="Durg","Durgapur"="Durgapur","Etawah"="Etawah","Faridabad"="Faridabad","Farrukhabad"="Farrukhabad","Firozabad"="Firozabad","Firozpur"="Firozpur","Gandhidham"="Gandhidham","Gauhati"="Gauhati","Gaya"="Gaya","Ghaziabad"="Ghaziabad","Gopalpur"="Gopalpur","Gorakhpur"="Gorakhpur","Guntur"="Guntur","Gurgaon"="Gurgaon","Guwahati"="Guwahati","Gwalior"="Gwalior","Hajipur"="Hajipur","Haldwani"="Haldwani","Hamirpur"="Hamirpur","Hapur"="Hapur","Haridwar"="Haridwar","Hinganghat"="Hinganghat","Hisar"="Hisar","Hoshiarpur"="Hoshiarpur","Howrah"="Howrah","Hyderabad"="Hyderabad","Ichalkaranji"="Ichalkaranji","Imphal"="Imphal","Indore"="Indore","Jabalpur"="Jabalpur","Jagadhri"="Jagadhri","Jaipur"="Jaipur","Jalandhar"="Jalandhar","Jalgaon"="Jalgaon","Jamnagar"="Jamnagar","Jamshedpur"="Jamshedpur","Jhansi"="Jhansi","Jind"="Jind","Jodhpur"="Jodhpur","Junagadh"="Junagadh","Kaithal"="Kaithal","Kakinada"="Kakinada","Kanpur"="Kanpur","Kapurthala"="Kapurthala","Karnal"="Karnal","Karnataka"="Karnataka","Kashipur"="Kashipur","Katihar"="Katihar","Khammam"="Khammam","Khanna"="Khanna","Kochi"="Kochi","Kolhapur"="Kolhapur","Kolkata"="Kolkata","Kollam"="Kollam","Korba"="Korba","Kota"="Kota","Kulti"="Kulti","Latur"="Latur","Loni"="Loni","Lucknow"="Lucknow","Ludhiana"="Ludhiana","Madurai"="Madurai","Malegaon"="Malegaon","Malerkotla"="Malerkotla","Mangalore"="Mangalore","Mango"="Mango","Manipur"="Manipur","Mathura"="Mathura","Mau"="Mau","Meerut"="Meerut","Meghalaya"="Meghalaya","Mirzapur"="Mirzapur","Moga"="Moga","Moradabad"="Moradabad","Muktsar"="Muktsar","Mumbai"="Mumbai","Munger"="Munger","Muzaffarnagar"="Muzaffarnagar","Muzaffarpur"="Muzaffarpur","Mysore"="Mysore","Nagpur"="Nagpur","Nanded"="Nanded","Nashik"="Nashik","Noida"="Noida","Pali"="Pali","Palwal"="Palwal","Panchkula"="Panchkula","Panipat"="Panipat","Panvel"="Panvel","Parbhani"="Parbhani","Pathankot"="Pathankot","Patiala"="Patiala","Patna"="Patna","Pimpri-Chinchwad"="Pimpri-Chinchwad","Poonch"="Poonch","Pune"="Pune","Purnia"="Purnia","Raipur"="Raipur","Rajkot"="Rajkot","Rampur"="Rampur","Ranchi"="Ranchi","Ratlam"="Ratlam","Rewa"="Rewa","Rishikesh"="Rishikesh","Rohtak"="Rohtak","Roorkee"="Roorkee","Rourkela"="Rourkela","Rudrapur"="Rudrapur","Sagar"="Sagar","Saharanpur"="Saharanpur","Saharsa"="Saharsa","Salem"="Salem","Satna"="Satna","Secunderabad"="Secunderabad","Shahjahanpur"="Shahjahanpur","Sikar"="Sikar","Siliguri"="Siliguri","Sirsa"="Sirsa","Solapur"="Solapur","Sonipat"="Sonipat","Srinagar"="Srinagar","Surat"="Surat","Tenali"="Tenali","Thane"="Thane","Thanesar"="Thanesar","Thanjavur"="Thanjavur","Thiruvananthapuram"="Thiruvananthapuram","Tirunelveli"="Tirunelveli","Tirupur"="Tirupur","Udaipur"="Udaipur","Udgir"="Udgir","Udhampur"="Udhampur","Ujjain"="Ujjain","Ulhasnagar"="Ulhasnagar","Vadodara"="Vadodara","Varanasi"="Varanasi","Visakhapatnam"="Visakhapatnam","Vizianagaram"="Vizianagaram","Warangal"="Warangal","Wardha"="Wardha","Yamunanagar"="Yamunanagar"          ), 
                            selected = 1), width=2
              ),
              mainPanel(
                p('After transforming the data through the map application and aggregating all the data by date and city we had a very nice dataset which could be used for plots. This dataset allowed us to shows plots per city in combination with the rain and in combination with the elevation (which will be discussed in the paragraph about elevation).'),
                p('With these plots we wanted to test the hypothesis if there is correlation between rainfall and tweets. We first created a plot which only shows the tweet. On the left side you can choose the city from which you want to see the tweets of and you can choose the time period. As you can see the y-axis is the amount of tweets and the x-axis is the date.'),
                p('Every city had some noise coming from them. Whether this was some event with the Hindi word of flooding in it or just some random noise because of ads, it didnt matter to us. All that matters is the spikes in the data. These spikes mean that there is an irregulation in the amount of tweets coming from a certain city and there will most likely be a flood or a flood is going on at that moment. So these spikes in correlation with the news articles are important, but because we do not collected a lot of news articles in that time period, we will only discuss the correlation with the rain in this chapter.'),
                plotOutput("Tweetspercity")
              )
            )
   ),
   tabPanel("Correlation",
            sidebarLayout(
              sidebarPanel(
                dateInput("BegindatumC", "Start Date:", value = "2014-07-23"),
                dateInput("EinddatumC", "End Date:", value = "2014-09-10"),width=2
              ),
              mainPanel(
                p(' A lot of rain does not always include a flood, but by logical reasoning a flood is more likely to occur. The next plot will show this correlation. When the raining becomes less, the tweeting becomes less and vice versa.'),
                plotOutput("Correlation"),
                img(src="legenda.png")
              )
            )
   ), 

    tabPanel("Rain",
             sidebarLayout(
               sidebarPanel(
                 dateInput("BBegindatum", "Start Date:", value = "2014-07-23"),
                 dateInput("EEinddatum", "End Date:", value = "2014-10-09"),
                 selectInput("checkGroup", label = h3("City"), 
                             choices = list("Abohar"="Abohar","Achalpur"="Achalpur","Adilabad"="Adilabad","Agartala"="Agartala","Agra"="Agra","Ahmedabad"="Ahmedabad","Ahmednagar"="Ahmednagar","Ajmer"="Ajmer","Aligarh"="Aligarh","Allahabad"="Allahabad","Alwar"="Alwar","Ambala"="Ambala","Ambernath"="Ambernath","Amravati"="Amravati","Amreli"="Amreli","Amritsar"="Amritsar","Anantapur"="Anantapur","Anantnag"="Anantnag","Arrah"="Arrah","Asansol"="Asansol","assam"="assam","Aurangabad"="Aurangabad","Badlapur"="Badlapur","Bahadurgarh"="Bahadurgarh","Bally"="Bally","Bangalore"="Bangalore","Barasat"="Barasat","Bardhaman"="Bardhaman","Bareilly"="Bareilly","Barnala"="Barnala","Batala"="Batala","Bathinda"="Bathinda","Begusarai"="Begusarai","Belgaum"="Belgaum","Bellary"="Bellary","Bhagalpur"="Bhagalpur","Bharatpur"="Bharatpur","Bhatpara"="Bhatpara","Bhavnagar"="Bhavnagar","Bhilai"="Bhilai","Bhilwara"="Bhilwara","Bhiwandi"="Bhiwandi","Bhiwani"="Bhiwani","Bhopal"="Bhopal","Bhubaneswar"="Bhubaneswar","Bhusawal"="Bhusawal","Bihar"="Bihar","Bijapur"="Bijapur","Bikaner"="Bikaner","Bilaspur"="Bilaspur","Bokaro"="Bokaro","Brahmapur"="Brahmapur","Bulandshahr"="Bulandshahr","Chandel"="Chandel","Chandigarh"="Chandigarh","Chandrapur"="Chandrapur","Chennai"="Chennai","Coimbatore"="Coimbatore","Cuttack"="Cuttack","Darbhanga"="Darbhanga","Dehradun"="Dehradun","Delhi"="Delhi","Dewas"="Dewas","Dhanbad"="Dhanbad","Dhubri"="Dhubri","Durg"="Durg","Durgapur"="Durgapur","Etawah"="Etawah","Faridabad"="Faridabad","Farrukhabad"="Farrukhabad","Firozabad"="Firozabad","Firozpur"="Firozpur","Gandhidham"="Gandhidham","Gauhati"="Gauhati","Gaya"="Gaya","Ghaziabad"="Ghaziabad","Gopalpur"="Gopalpur","Gorakhpur"="Gorakhpur","Guntur"="Guntur","Gurgaon"="Gurgaon","Guwahati"="Guwahati","Gwalior"="Gwalior","Hajipur"="Hajipur","Haldwani"="Haldwani","Hamirpur"="Hamirpur","Hapur"="Hapur","Haridwar"="Haridwar","Hinganghat"="Hinganghat","Hisar"="Hisar","Hoshiarpur"="Hoshiarpur","Howrah"="Howrah","Hyderabad"="Hyderabad","Ichalkaranji"="Ichalkaranji","Imphal"="Imphal","Indore"="Indore","Jabalpur"="Jabalpur","Jagadhri"="Jagadhri","Jaipur"="Jaipur","Jalandhar"="Jalandhar","Jalgaon"="Jalgaon","Jamnagar"="Jamnagar","Jamshedpur"="Jamshedpur","Jhansi"="Jhansi","Jind"="Jind","Jodhpur"="Jodhpur","Junagadh"="Junagadh","Kaithal"="Kaithal","Kakinada"="Kakinada","Kanpur"="Kanpur","Kapurthala"="Kapurthala","Karnal"="Karnal","Karnataka"="Karnataka","Kashipur"="Kashipur","Katihar"="Katihar","Khammam"="Khammam","Khanna"="Khanna","Kochi"="Kochi","Kolhapur"="Kolhapur","Kolkata"="Kolkata","Kollam"="Kollam","Korba"="Korba","Kota"="Kota","Kulti"="Kulti","Latur"="Latur","Loni"="Loni","Lucknow"="Lucknow","Ludhiana"="Ludhiana","Madurai"="Madurai","Malegaon"="Malegaon","Malerkotla"="Malerkotla","Mangalore"="Mangalore","Mango"="Mango","Manipur"="Manipur","Mathura"="Mathura","Mau"="Mau","Meerut"="Meerut","Meghalaya"="Meghalaya","Mirzapur"="Mirzapur","Moga"="Moga","Moradabad"="Moradabad","Muktsar"="Muktsar","Mumbai"="Mumbai","Munger"="Munger","Muzaffarnagar"="Muzaffarnagar","Muzaffarpur"="Muzaffarpur","Mysore"="Mysore","Nagpur"="Nagpur","Nanded"="Nanded","Nashik"="Nashik","Noida"="Noida","Pali"="Pali","Palwal"="Palwal","Panchkula"="Panchkula","Panipat"="Panipat","Panvel"="Panvel","Parbhani"="Parbhani","Pathankot"="Pathankot","Patiala"="Patiala","Patna"="Patna","Pimpri-Chinchwad"="Pimpri-Chinchwad","Poonch"="Poonch","Pune"="Pune","Purnia"="Purnia","Raipur"="Raipur","Rajkot"="Rajkot","Rampur"="Rampur","Ranchi"="Ranchi","Ratlam"="Ratlam","Rewa"="Rewa","Rishikesh"="Rishikesh","Rohtak"="Rohtak","Roorkee"="Roorkee","Rourkela"="Rourkela","Rudrapur"="Rudrapur","Sagar"="Sagar","Saharanpur"="Saharanpur","Saharsa"="Saharsa","Salem"="Salem","Satna"="Satna","Secunderabad"="Secunderabad","Shahjahanpur"="Shahjahanpur","Sikar"="Sikar","Siliguri"="Siliguri","Sirsa"="Sirsa","Solapur"="Solapur","Sonipat"="Sonipat","Srinagar"="Srinagar","Surat"="Surat","Tenali"="Tenali","Thane"="Thane","Thanesar"="Thanesar","Thanjavur"="Thanjavur","Thiruvananthapuram"="Thiruvananthapuram","Tirunelveli"="Tirunelveli","Tirupur"="Tirupur","Udaipur"="Udaipur","Udgir"="Udgir","Udhampur"="Udhampur","Ujjain"="Ujjain","Ulhasnagar"="Ulhasnagar","Vadodara"="Vadodara","Varanasi"="Varanasi","Visakhapatnam"="Visakhapatnam","Vizianagaram"="Vizianagaram","Warangal"="Warangal","Wardha"="Wardha","Yamunanagar"="Yamunanagar"          ), 
                             selected = 1), width=2
               ),
               mainPanel(
                 p('The rain plot is pretty easy to read. On the x-axis there is the date and the y-axis there is the rain. The same analysis as stated for the tweet count can be used here. When there is a spike, that city is more prone to flooding.'),
                 plotOutput("Rainpercity")
               )
             )
    ),

  tabPanel("Rain and tweets",
           sidebarLayout(
             sidebarPanel(
               dateInput("BegindatumR", "Start Date:", value = "2014-07-23"),
               dateInput("EinddatumR", "End Date:", value = "2014-10-09"),
               selectInput("checkGroupR", label = h3("City"), 
                           choices = list("Abohar"="Abohar","Achalpur"="Achalpur","Adilabad"="Adilabad","Agartala"="Agartala","Agra"="Agra","Ahmedabad"="Ahmedabad","Ahmednagar"="Ahmednagar","Ajmer"="Ajmer","Aligarh"="Aligarh","Allahabad"="Allahabad","Alwar"="Alwar","Ambala"="Ambala","Ambernath"="Ambernath","Amravati"="Amravati","Amreli"="Amreli","Amritsar"="Amritsar","Anantapur"="Anantapur","Anantnag"="Anantnag","Arrah"="Arrah","Asansol"="Asansol","assam"="assam","Aurangabad"="Aurangabad","Badlapur"="Badlapur","Bahadurgarh"="Bahadurgarh","Bally"="Bally","Bangalore"="Bangalore","Barasat"="Barasat","Bardhaman"="Bardhaman","Bareilly"="Bareilly","Barnala"="Barnala","Batala"="Batala","Bathinda"="Bathinda","Begusarai"="Begusarai","Belgaum"="Belgaum","Bellary"="Bellary","Bhagalpur"="Bhagalpur","Bharatpur"="Bharatpur","Bhatpara"="Bhatpara","Bhavnagar"="Bhavnagar","Bhilai"="Bhilai","Bhilwara"="Bhilwara","Bhiwandi"="Bhiwandi","Bhiwani"="Bhiwani","Bhopal"="Bhopal","Bhubaneswar"="Bhubaneswar","Bhusawal"="Bhusawal","Bihar"="Bihar","Bijapur"="Bijapur","Bikaner"="Bikaner","Bilaspur"="Bilaspur","Bokaro"="Bokaro","Brahmapur"="Brahmapur","Bulandshahr"="Bulandshahr","Chandel"="Chandel","Chandigarh"="Chandigarh","Chandrapur"="Chandrapur","Chennai"="Chennai","Coimbatore"="Coimbatore","Cuttack"="Cuttack","Darbhanga"="Darbhanga","Dehradun"="Dehradun","Delhi"="Delhi","Dewas"="Dewas","Dhanbad"="Dhanbad","Dhubri"="Dhubri","Durg"="Durg","Durgapur"="Durgapur","Etawah"="Etawah","Faridabad"="Faridabad","Farrukhabad"="Farrukhabad","Firozabad"="Firozabad","Firozpur"="Firozpur","Gandhidham"="Gandhidham","Gauhati"="Gauhati","Gaya"="Gaya","Ghaziabad"="Ghaziabad","Gopalpur"="Gopalpur","Gorakhpur"="Gorakhpur","Guntur"="Guntur","Gurgaon"="Gurgaon","Guwahati"="Guwahati","Gwalior"="Gwalior","Hajipur"="Hajipur","Haldwani"="Haldwani","Hamirpur"="Hamirpur","Hapur"="Hapur","Haridwar"="Haridwar","Hinganghat"="Hinganghat","Hisar"="Hisar","Hoshiarpur"="Hoshiarpur","Howrah"="Howrah","Hyderabad"="Hyderabad","Ichalkaranji"="Ichalkaranji","Imphal"="Imphal","Indore"="Indore","Jabalpur"="Jabalpur","Jagadhri"="Jagadhri","Jaipur"="Jaipur","Jalandhar"="Jalandhar","Jalgaon"="Jalgaon","Jamnagar"="Jamnagar","Jamshedpur"="Jamshedpur","Jhansi"="Jhansi","Jind"="Jind","Jodhpur"="Jodhpur","Junagadh"="Junagadh","Kaithal"="Kaithal","Kakinada"="Kakinada","Kanpur"="Kanpur","Kapurthala"="Kapurthala","Karnal"="Karnal","Karnataka"="Karnataka","Kashipur"="Kashipur","Katihar"="Katihar","Khammam"="Khammam","Khanna"="Khanna","Kochi"="Kochi","Kolhapur"="Kolhapur","Kolkata"="Kolkata","Kollam"="Kollam","Korba"="Korba","Kota"="Kota","Kulti"="Kulti","Latur"="Latur","Loni"="Loni","Lucknow"="Lucknow","Ludhiana"="Ludhiana","Madurai"="Madurai","Malegaon"="Malegaon","Malerkotla"="Malerkotla","Mangalore"="Mangalore","Mango"="Mango","Manipur"="Manipur","Mathura"="Mathura","Mau"="Mau","Meerut"="Meerut","Meghalaya"="Meghalaya","Mirzapur"="Mirzapur","Moga"="Moga","Moradabad"="Moradabad","Muktsar"="Muktsar","Mumbai"="Mumbai","Munger"="Munger","Muzaffarnagar"="Muzaffarnagar","Muzaffarpur"="Muzaffarpur","Mysore"="Mysore","Nagpur"="Nagpur","Nanded"="Nanded","Nashik"="Nashik","Noida"="Noida","Pali"="Pali","Palwal"="Palwal","Panchkula"="Panchkula","Panipat"="Panipat","Panvel"="Panvel","Parbhani"="Parbhani","Pathankot"="Pathankot","Patiala"="Patiala","Patna"="Patna","Pimpri-Chinchwad"="Pimpri-Chinchwad","Poonch"="Poonch","Pune"="Pune","Purnia"="Purnia","Raipur"="Raipur","Rajkot"="Rajkot","Rampur"="Rampur","Ranchi"="Ranchi","Ratlam"="Ratlam","Rewa"="Rewa","Rishikesh"="Rishikesh","Rohtak"="Rohtak","Roorkee"="Roorkee","Rourkela"="Rourkela","Rudrapur"="Rudrapur","Sagar"="Sagar","Saharanpur"="Saharanpur","Saharsa"="Saharsa","Salem"="Salem","Satna"="Satna","Secunderabad"="Secunderabad","Shahjahanpur"="Shahjahanpur","Sikar"="Sikar","Siliguri"="Siliguri","Sirsa"="Sirsa","Solapur"="Solapur","Sonipat"="Sonipat","Srinagar"="Srinagar","Surat"="Surat","Tenali"="Tenali","Thane"="Thane","Thanesar"="Thanesar","Thanjavur"="Thanjavur","Thiruvananthapuram"="Thiruvananthapuram","Tirunelveli"="Tirunelveli","Tirupur"="Tirupur","Udaipur"="Udaipur","Udgir"="Udgir","Udhampur"="Udhampur","Ujjain"="Ujjain","Ulhasnagar"="Ulhasnagar","Vadodara"="Vadodara","Varanasi"="Varanasi","Visakhapatnam"="Visakhapatnam","Vizianagaram"="Vizianagaram","Warangal"="Warangal","Wardha"="Wardha","Yamunanagar"="Yamunanagar"          ), 
                           selected = 1), width=2
             ),
             mainPanel(
               p('The next plot shows both previous explained plots of the rain and the tweets per city in one graph. But what do you look for in this plot: If there is a spike in rainfall and there is a spike in tweets on the same day, a flood likely occurred.'),
               plotOutput("RainTweets"),
               img(src="legenda.png")
             )
           )
  )
  ),
  
    tabPanel("Population with Tweets",
      mainPanel(
        img(src="PlotCountTweetsPop.jpg"),
        p('We have calculated the amount of tweets and compared it with the news data to find correlations. Now we are going to analyze the accuracy of the tweets, because in our observations we have noticed a lot of tweets are coming from big cities like Delhi and Mumbai. When we have filtered the tweets, we have found more than 200 cities that were mentioned in some way. In this plot we have taken a sample of twenty cities.'),
        p('We have calculated the population by city and the average tweets by city in 2014 from the tweets from Floodtags. How higher the point, the larger the population is and how bigger the red point is, how larger the average tweets are by population of that city. So take as example Bihar, they have a low populations compared to the other ones, but a really high average(0.38 tweet about flooding per inhabitant).'),
        p('We have compared the news data and the rainfall data with tweets to determine whether the city Bihar was really affected by floods. In Bihar there were two major floods, one in the beginning of august by a landslide in Nepal and one flood midway august.'),
        p('- http://timesofindia.indiatimes.com/india/Kosi-flood-alert-Bihar-evacuates-50000-people/articleshow/39577228.cms'),
        p('- http://www.livemint.com/Politics/Rt1VR7H9sEN1r20CUKn8wO/Flood-situation-grim-in-parts-of-Uttar-Pradesh-and-Bihar.html'),
        p('So there were definitely floods in Bihar according to the news.')

      )
  ),
    

    


      
  navbarMenu("Elevation",
    tabPanel("General",
      mainPanel(
        p('First of all there is the plot in the tab General. This plot below shows the amount of cities per population category.'),
        plotOutput("Elevation"),
        p('Let start with explaining what the Risk Areas actually are before showing the plots. The elevation data to create these plots were extracted for large hgt files of the whole of India. Becomes this was several gigabytes big it was very hard to do complex comparisons within this data. At first, the idea was to check if a specific point on the map had a very big difference in height with a point nearby which was at ground level. This point would then be defined as a point where a steep hill would  be. Because our hypothesis was: living around a mountain creates a bigger risk of floodings. To create this algorithm and let it run over all the gigabytes of data would take a very long time, probably only to let it run would take several days. Therefor we tried a different approach: we found datapoints which are between 300 and 500 meters and worked from there. This would greatly reduce the processing time and the time to make the algorithm.The idea of these data points is: India has very high mountains where the top is higher than 500 meters. If we find the points that are between 300 and 500 meters, there is a very big change this is the around the foot of a hill. If we lay the next picture, next to our later defined risk areas this actually worked pretty well. The conclusion is that we choose time over accuracy, but still got a pretty good result.'),
        img(src="riskareas.png")
      )
    ),
    tabPanel("Risk area normal",
       mainPanel(
         p('After we found these points we added and subtracted 0.05 of the lat to create a left lat point and a right lat point. We did the same for the lon to create a square. This square is the risk area we talk about in this report. We created these squares so we had an area where the cities could be in which would be at the foot of a hill. Because our hypothesis was: cities that are close to a mountain, are more prone to flooding.'),
         plotOutput("Elevationnnnn"),
         p('This first plot shows on the x axis if a city is in a risk area or not and the y axis shows how many cities are in this specific category. This barplot shows that there are more cities which lie in the NoRisk areas than in the Risk areas.'),
         plotOutput("Elevationnnnnnnn"),
         p('After that we made a barplot to show how many tweets there are in the NoRisk and in the Risk area. This barplot is split per Risk category in population category. Because there are not a lot of cities falling in this risk category we do not go in much detail about this plot.'),
         plotOutput("elevationrain"),
         p('This plot shows the average amount of rain of both the rain in the risk areas (blue) as the rain which did not appear in the risk areas (red). From this plots we can analysis big differences between both categories. For example if on a specific day there was a lot more rain in the risk areas, we can probably conclude that a lot of cities in surrounding risk areas had problems with floodings. 
After some research we concluded that the risk areas are too small defined. Water travels for a long time and especially when the water comes from a mountain. So we subtracted and added 0.05 to the risc area on the lat and the lon.')
       )
    ),
    tabPanel("Risk big (+0.05)",
      mainPanel(
        p('The next plots are off this new risk areas. As you can see the amount of cities per category is more evenly divided. In the second plot we can see that in the risk areas there are a lot of cities with middle to big population numbers and in the not risk areas there are a lot of very big cities. With this we can conclude that very big cities do not settle at the foot of a mountain.'),
        plotOutput("Elevationnnnnn"),
        p('After that we wanted to research if the cities who are not in a risk area are correlated with the distance that they are from a risk area. The following plot shows this correlation.'),
        plotOutput("EElevation"),
        plotOutput("elevationrainn")
      )
    ),
    tabPanel("Conclusion",
      mainPanel(
        plotOutput("km"),
        p('As you can see it shows that the farther a city is from a risk area, the less tweets per inhabitants there is. So this hypothesis is proven to be right with this specific plot.'),
        p('The hypothesis if cities who lie close to the foot of a mountain are more likely to flood is not proven to be right with these plots. We did not find strong proof that a lot more tweets occur in the risk areas and we did not find sufficient proof that the distance from a risk areas is correlated with less or more tweets. Still there was some correlation found, but it is very hard to determine if that was just luck or that the risk areas actually worked, because we only got the tweets of three months in 2014.'),
        p('A probable cause of the fact that the risk areas do not work is we underestimated the “reach” of the rain. For example Bihar still had a very large flood because of rain high in the Nepal region. This means that sometimes the definition of the risk areas were set too small which concluded in cities falling outside of the risk area, but were still very prone to flood because of elevation differences.')
      )
    )
  ),
  
    tabPanel("Source",
           mainPanel(
             p(strong('When a flood occurs, the sources most used to tweet about floods will be mobile clients')),
             p('To answer this hypothesis we first looked at the distribution of sources used to Tweet about floods. Right off the spot there seems to be a huge favor for mobile clients, regardless of floods occurring or not. Sadly, there wasn’t any significant difference between flood sources and non-flood sources. It was also hard to visualize since the data we wanted to combine wasnt really compatible. We can however say for certain that there is a huge favour for mobile clients in India.'),
             plotOutput("SourceTwee"),
             p('We cannot really prove this hypothesis, but we can still say for certain that mobile clients do indeed have a huge favor over any other clients.')
           )
    ),
  
    navbarMenu("Meta Old",
      tabPanel("Created",
        mainPanel(
          p('Created is the attribute which stores the creation date of each Tweet, which basically means when a tweet was written and published.'),
          plotOutput("Created"),
          p('Since this plot solely focuses on the creation date (per the written language of the Tweet) we cannot say if this attribute will prove itself useful for identifying floods. However, we do see peaks and lows which means the data is somehow affected by another attribute.')
          
        )
      ),
      tabPanel("Source",
        mainPanel(
          plotOutput("Source"),
          p('With this sources frequency plot, we looked at which sources were used mostly for publishing the Tweets in our datasource. We clearly see a favor for mobile clients.')
        )
      ),
      tabPanel("Country",
        mainPanel(
          p('We expected to see a huge bar for India, since we looked for Tweets from India. However, it surprised us to see that for such a huge number of Tweets, the country attribute was unknown. This would later prove a challenge since we wanted to work with Tweets based on locations.'),
          plotOutput("Country")
        )
      ),  
      tabPanel("Timezone",
        mainPanel(
          p('Since the country attribute alone would presumable supply too little data to work with Tweets on locations, we looked for other ways of identifying if Tweets actually did come from India or were posted from somewhere else in the world.'),
          plotOutput("Timezone"),
          p('We found more promising data in the timezone attribute. Here we plot the number of users per time zone, but we could easily change this to tweets per timezone.')
        )
      ),
      tabPanel("Language",
        mainPanel(
          p('When looking through the data source when identifying useful attributes, we found that a lot of Tweets were written in a Hindi-like language. This made it very hard to actually see what the Tweet was about and filter out noise or misleading Tweets. To get an idea of the size of hindi-based Tweets and Tweets written in any other language, we made a visualisation of the written language of each Tweet (ISO language code).'),
          plotOutput("Language")
        )
      ),
      tabPanel("ISO",
        mainPanel(
          p('The plot clearly shows that the majority of Tweets are written in NE (Nepali) and HI (Hindi) and only tiny amount of Tweets are actually in EN (English).'),
          plotOutput("Isocode")
        )
      ) 
    )
  )
)