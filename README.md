FloodFlamingos

ToDo:

- Loop through Tweets to filter based on translated cities from cities.json

- Add heat map with height

- Resolve bug that doesn't update the popup windows on geolocated news articles

- Research implementation of RSS news feed or news API

- Clean data / tweets

- Filter news based on date (create buttons?)

- Start predicting

- Difference with tweets and floods