<?php
  set_time_limit(0);
  
  // New getTweets for indiaNew.json file
  function getTweets() {
    // Looks at the translated cities and at the text in the tweets.
    $citiesJson = file_get_contents("./cities.json");
    $citiesData = json_decode($citiesJson, TRUE);

    $tweets = file("indiaNew.json");

    $tweetText = array();    
    $x = 1;
    foreach($tweets as $line) {
        $tweetText[$x] = json_decode($line, TRUE);
        $x = $x + 1;
    }

    $returnArray = array();
    $count = 0;
    foreach ($tweetText as $key => $value) {
      foreach ($citiesData as $cities => $city) {
        if(strpos($value['text'], $city['cityH']) !== false) {
          $count = $count+1;
          $returnArray[$count]['city'] = $city['city'];
          $returnArray[$count]['lat'] = $city['lat'];
          $returnArray[$count]['lng'] = $city['lng'];
          $returnArray[$count]['tweets'] = 0;
          continue;
        }
      }
    }

    foreach ($returnArray as $key => $value) {
      foreach ($returnArray as $key2 => $value2) {
        if ($value['city'] == $value2['city']) {
          $returnArray[$key2]['tweets'] = $returnArray[$key2]['tweets'] + 1;
        }
      }
    }
    echo json_encode($returnArray);
  }
?>


<html>
  <head>
    <title>TwitterFloods - FloodFlamingos</title>
    <script>

      var tweetCount = "lat,lng,tweetCount,city";
      var markerCluster; 
      var markers = [];

      function downloadMarkerData(contents) {
        var textFile = null,
        makeTextFile = function (text) {
          var data = new Blob([text], {type: 'csv'});

          if (textFile !== null) {
            window.URL.revokeObjectURL(textFile);
          }

          textFile = window.URL.createObjectURL(data);

          return textFile;
        };

        var create = document.getElementById('create');

        create.addEventListener('click', function () {
          var link = document.getElementById('downloadlink');
          link.href = makeTextFile(contents);
          link.style.display = 'block';
        }, false);
      };

      
      // Sets all the markers on the map (for tweets & news articles)
      function setMarkers() {
        var markersPhp = <?php getTweets() ?>;
        
        var latPrevious = "";
        // Add the tweets
        for (var i in markersPhp) {
          var lat = markersPhp[i]['lat'];
          var lng = markersPhp[i]['lng'];
          var city = markersPhp[i]['city'];
          var count = markersPhp[i]['tweets'];

          if (lat != latPrevious) {
            tweetCount = tweetCount + "\n" + lat + "," + lng + "," + count + "," + city;
          }
          latPrevious = lat;
        }
      }
    </script>
  </head>

  <body>
    <h3>&nbsp;FloodFlamingos</h3>
    <div>
      &nbsp;<a download="tweetCount.csv" id="downloadlink" style="display: none">Download generated tweetCount.csv file</a>
      &nbsp;<button onClick="setMarkers();downloadMarkerData(tweetCount);" id="create">Generate tweetCount data (for Mitchell) (doubleclick)</button>
    </div>
  </body>
</html>