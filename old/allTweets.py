import json

if __name__ == '__main__':
    
    cities = json.load(open("cities.json"));
    tweets = open("/media/rob/BACKUP_MEDIA/twitterFloods/india.json", "r");
    citiesDictionary = {}
    countLines = 1
       
    # loop through tweets
    for line in tweets:
        tweet_json = json.loads(line)["text"]
        tweet_date_json = json.loads(line)["date"]["$date"]
        if countLines % 10000 == 0:
            print countLines
            # convert to string to output to .csv
            output = 'city,lat,lng,date'
            for city in citiesDictionary:
                output += '\n' + citiesDictionary[city][0] + ',' + citiesDictionary[city][1] + ',' + citiesDictionary[city][2] + ',' + citiesDictionary[city][3]
            f = open("/media/rob/BACKUP_MEDIA/twitterFloods/allTweetsWithDates.csv", "rw+")
            f.truncate()
            f.write(output)
            f.close()
        countLines = countLines + 1
        # loop through words in tweets
        for word in tweet_json.split(" "):
            # loop through cities
            for city in cities:
                # compare words in tweets with cities and add to dictionary
                if word == cities[city]['cityH']:
                    citiesDictionary[str(cities[city]['city'])] = [str(cities[city]['city']), str(cities[city]['lat']), str(cities[city]['lng']), str(tweet_date_json)[:10]]
