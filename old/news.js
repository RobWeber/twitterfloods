var news = {
  "India flood victims find destruction" :     {
    "Title" : "India flood victims find destruction 123",
    "Link" : "http://edition.cnn.com/2009/WORLD/asiapcf/10/07/india.aftermath/index.html",
    "Location" : "Kurnool",
    "Date" : "Wed Oct 07 2009 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "",
    "People affected" : "",
    "Rescued" : ""
  },
  "4 Dead, 6.5 Lakh Displaced in Assam Floods, Heavy Rains Hamper Rescue Ops" :     {
    "Title" : "4 Dead, 6.5 Lakh Displaced in Assam Floods, Heavy Rains Hamper Rescue Ops",
    "Link" : "http://www.ndtv.com/india-news/4-dead-6-5-lakh-displaced-in-assam-floods-heavy-rains-hamper-rescue-ops-1209971",
    "Location" : "Morigoan",
    "Date" : "Wed Sep 23 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "",
    "People affected" : "",
    "Rescued" : ""
  },
  "Over 1.5 million people hit by floods in Assam" :     {
    "Title" : "Over 1.5 million people hit by floods in Assam",
    "Link" : "http://in.reuters.com/article/2015/09/07/india-floods-assam-idINKCN0R71NE20150907",
    "Location" : "Morigaon",
    "Date" : "Mon Sep 07 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "50",
    "People affected" : "",
    "Rescued" : ""
  },
  "Heavy rainfall lashes Tamil Nadu, 71 killed so far; Jayalalithaa’s convoy stuck due to waterlogging" :     {
    "Title" : "Heavy rainfall lashes Tamil Nadu, 71 killed so far; Jayalalithaa’s convoy stuck due to waterlogging",
    "Link" : "http://indianexpress.com/article/india/india-news-india/heavy-rainfall-throws-life-out-of-gear-in-tamil-nadu-traffic-badly-hit-71-killed-so-far/",
    "Location" : "Chennai",
    "Date" : "Sat Oct 17 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "71",
    "People affected" : "",
    "Rescued" : ""
  },
  "Flooding in southern India kills dozens" :     {
    "Title" : "Flooding in southern India kills dozens",
    "Link" : "http://www.aljazeera.com/news/2015/11/flooding-southern-india-kills-dozens-151114095100723.html",
    "Location" : "Chennai",
    "Date" : "Sat Nov 14 2015 03:00:00 GMT-0500 (EST)",
    "Rainfall" : "492",
    "Died" : "55",
    "People affected" : "",
    "Rescued" : ""
  },
  "Mumbai will likely flood again – and nobody's doing much about it" :     {
    "Title" : "Mumbai will likely flood again – and nobody's doing much about it",
    "Link" : "http://www.theguardian.com/cities/2014/nov/27/mumbai-flood-rain-monsoon-city-planning",
    "Location" : "Mumbai",
    "Date" : "Fri Jun 26 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "944",
    "Died" : "",
    "People affected" : "",
    "Rescued" : ""
  },
  "India – Tamil Nadu Floods Continue as Death Toll Rises" :     {
    "Title" : "India – Tamil Nadu Floods Continue as Death Toll Rises",
    "Link" : "http://floodlist.com/asia/india-tamil-nadu-floods-continue-as-death-toll-rises",
    "Location" : "Chennai",
    "Date" : "Mon Nov 16 2015 03:00:00 GMT-0500 (EST)",
    "Rainfall" : "184",
    "Died" : "27",
    "People affected" : "",
    "Rescued" : ""
  },
  "India – Assam Floods Death Toll Rises but Flooding Recedes in Some Districts" :     {
    "Title" : "India – Assam Floods Death Toll Rises but Flooding Recedes in Some Districts",
    "Link" : "http://floodlist.com/asia/india-assam-floods-death-toll-rises-but-flooding-recedes-in-some-districts",
    "Location" : "Morigaon",
    "Date" : "Thu Sep 10 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "42",
    "People affected" : "210000",
    "Rescued" : ""
  },
  "India – Floods Return to Assam Leaving Over 20 Dead" :     {
    "Title" : "India – Floods Return to Assam Leaving Over 20 Dead",
    "Link" : "http://floodlist.com/asia/india-floods-assam-september-2015",
    "Location" : "Assam",
    "Date" : "Tue Sep 01 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "21",
    "People affected" : "736000",
    "Rescued" : ""
  },
  "13 Dead, 600,000 Affected as More Floods Hit Assam" :     {
    "Title" : "13 Dead, 600,000 Affected as More Floods Hit Assam",
    "Link" : "http://floodlist.com/asia/india-13-dead-600000-affected-as-more-floods-hit-assam",
    "Location" : "Assam",
    "Date" : "Mon Aug 24 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "13",
    "People affected" : "600000",
    "Rescued" : ""
  },
  "India Floods – 5 Dead in Nagpur, Maharashtra, After 250 mm of Rain in 36 Hours" :     {
    "Title" : "India Floods – 5 Dead in Nagpur, Maharashtra, After 250 mm of Rain in 36 Hours",
    "Link" : "http://floodlist.com/asia/india-floods-5-dead-nagpur-maharashtra",
    "Location" : "Nagpur",
    "Date" : "Fri Aug 14 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "250",
    "Died" : "5",
    "People affected" : "",
    "Rescued" : "1500"
  },
  "India Floods – 500,000 Affected in Manipur" :     {
    "Title" : "India Floods – 500,000 Affected in Manipur",
    "Link" : "http://floodlist.com/asia/india-floods-500000-affected-manipur",
    "Location" : "Manipur",
    "Date" : "Thu Aug 06 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "69",
    "People affected" : "259000",
    "Rescued" : ""
  },
  "India – Floods in 5 States Leave Over 80 Dead" :     {
    "Title" : "India – Floods in 5 States Leave Over 80 Dead",
    "Link" : "http://floodlist.com/asia/india-floods-rajasthan-gujurat-west-bengal-odisha-manipur",
    "Location" : "Rajkot",
    "Date" : "Tue Jul 28 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "423",
    "Died" : "22",
    "People affected" : "",
    "Rescued" : ""
  },
  "India – Floods in 5 States Leave Over 80 Dead" :     {
    "Title" : "India – Floods in 5 States Leave Over 80 Dead",
    "Link" : "http://floodlist.com/asia/india-floods-rajasthan-gujurat-west-bengal-odisha-manipur",
    "Location" : "Bikaner",
    "Date" : "Sun Aug 02 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "363",
    "Died" : "28",
    "People affected" : "",
    "Rescued" : ""
  },
  "India – Floods in 5 States Leave Over 80 Dead" :     {
    "Title" : "India – Floods in 5 States Leave Over 80 Dead",
    "Link" : "http://floodlist.com/asia/india-floods-rajasthan-gujurat-west-bengal-odisha-manipur",
    "Location" : "Chandel",
    "Date" : "Sat Aug 01 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "20",
    "People affected" : "",
    "Rescued" : "22"
  },
  "Floods Kill Dozens in Gujarat, India, after Torrential Monsoon Rain" :     {
    "Title" : "Floods Kill Dozens in Gujarat, India, after Torrential Monsoon Rain",
    "Link" : "http://floodlist.com/asia/floods-gujarat-india-monsoon-rain",
    "Location" : "Amreli",
    "Date" : "Sat Jul 25 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "200",
    "Died" : "30",
    "People affected" : "",
    "Rescued" : "1000"
  },
  "India – Assam Floods Worsen – 700 Villages Affected as Brahmaputra Levels Rise" :     {
    "Title" : "India – Assam Floods Worsen – 700 Villages Affected as Brahmaputra Levels Rise",
    "Link" : "http://floodlist.com/asia/india-assam-floods-worsen-brahmaputra-rises",
    "Location" : "Dhubri",
    "Date" : "Sun Jul 12 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "36",
    "Died" : "",
    "People affected" : "",
    "Rescued" : ""
  },
  "India – Assam Floods Leave 5 More Dead as Brahmaputra and Tributaries Remain Above Danger Level" :     {
    "Title" : "India – Assam Floods Leave 5 More Dead as Brahmaputra and Tributaries Remain Above Danger Level",
    "Link" : "http://floodlist.com/asia/india-assam-floods-leave-5-more-dead-as-brahmaputra-and-tributaries-remain-above-danger-level",
    "Location" : "Meghalaya",
    "Date" : "Fri Aug 07 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "",
    "People affected" : "40000",
    "Rescued" : ""
  },
  "India – Assam Floods Leave 1 Dead, 9,000 Displaced" :     {
    "Title" : "India – Assam Floods Leave 1 Dead, 9,000 Displaced",
    "Link" : "http://floodlist.com/asia/india-assam-floods-1-dead-9000-displaced",
    "Location" : "Bongaigaon",
    "Date" : "Mon Jun 08 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "1",
    "People affected" : "",
    "Rescued" : ""
  },
  "At Least 6 Killed in Flash Floods in Tamil Nadu, India" :     {
    "Title" : "At Least 6 Killed in Flash Floods in Tamil Nadu, India",
    "Link" : "http://floodlist.com/asia/6-killed-flash-floods-sathuragiri-tamil-nadu",
    "Location" : "Virudhunagar",
    "Date" : "Fri May 08 2015 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "6",
    "People affected" : "",
    "Rescued" : ""
  },
  "Northeast Monsoon Floods Claim 5 Lives in Southern India" :     {
    "Title" : "Northeast Monsoon Floods Claim 5 Lives in Southern India",
    "Link" : "http://floodlist.com/asia/northeast-monsoon-floods-claim-5-lives-southern-india",
    "Location" : "Karnataka",
    "Date" : "Sat Oct 25 2014 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "69",
    "Died" : "",
    "People affected" : "",
    "Rescued" : ""
  },
  "Northeast Monsoon Floods Claim 5 Lives in Southern India" :     {
    "Title" : "Northeast Monsoon Floods Claim 5 Lives in Southern India",
    "Link" : "http://floodlist.com/asia/northeast-monsoon-floods-claim-5-lives-southern-india",
    "Location" : "Nellore",
    "Date" : "Sun Oct 26 2014 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "90",
    "Died" : "",
    "People affected" : "",
    "Rescued" : ""
  },
  "Northeast Monsoon Floods Claim 5 Lives in Southern India" :     {
    "Title" : "Northeast Monsoon Floods Claim 5 Lives in Southern India",
    "Link" : "http://floodlist.com/asia/northeast-monsoon-floods-claim-5-lives-southern-india",
    "Location" : "Tirupur",
    "Date" : "Mon Oct 27 2014 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "",
    "People affected" : "300",
    "Rescued" : ""
  },
  "1 Week of Flooding Leaves 88 Dead in Meghalaya and Assam, India" :     {
    "Title" : "1 Week of Flooding Leaves 88 Dead in Meghalaya and Assam, India",
    "Link" : "http://floodlist.com/asia/floods-88-dead-meghalaya-assam-india",
    "Location" : "Gauhati",
    "Date" : "Mon Sep 29 2014 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "203",
    "Died" : "88",
    "People affected" : "1000000",
    "Rescued" : "6000"
  },
  "India and Pakistan Floods – Rescue and Relief Operations Continue" :     {
    "Title" : "India and Pakistan Floods – Rescue and Relief Operations Continue",
    "Link" : "http://floodlist.com/asia/india-pakistan-floods-relief-operations",
    "Location" : "Jammu and Kashmir",
    "Date" : "Tue Sep 09 2014 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "37",
    "People affected" : "400000",
    "Rescued" : "25000"
  },
  "Floods Across Northern India – 6 States Affected" :     {
    "Title" : "Floods Across Northern India – 6 States Affected",
    "Link" : "http://floodlist.com/asia/floods-across-northern-india-6-states-affected",
    "Location" : "Uttar Pradesh",
    "Date" : "Sat Aug 16 2014 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "41",
    "People affected" : "",
    "Rescued" : ""
  },
  "Floods Across Northern India – 6 States Affected" :     {
    "Title" : "Floods Across Northern India – 6 States Affected",
    "Link" : "http://floodlist.com/asia/floods-across-northern-india-6-states-affected",
    "Location" : "Uttarakhand",
    "Date" : "Sat Aug 16 2014 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "50",
    "People affected" : "",
    "Rescued" : ""
  },
  "Floods Across Northern India – 6 States Affected" :     {
    "Title" : "Floods Across Northern India – 6 States Affected",
    "Link" : "http://floodlist.com/asia/floods-across-northern-india-6-states-affected",
    "Location" : "Bihar",
    "Date" : "Sat Aug 16 2014 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "10",
    "People affected" : "",
    "Rescued" : ""
  },
  "Floods Across Northern India – 6 States Affected" :     {
    "Title" : "Floods Across Northern India – 6 States Affected",
    "Link" : "http://floodlist.com/asia/floods-across-northern-india-6-states-affected",
    "Location" : "Assam",
    "Date" : "Sat Aug 16 2014 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "1",
    "People affected" : "35000",
    "Rescued" : ""
  },
  "Floods Across Northern India – 6 States Affected" :     {
    "Title" : "Floods Across Northern India – 6 States Affected",
    "Link" : "http://floodlist.com/asia/floods-across-northern-india-6-states-affected",
    "Location" : "Arunachal Pradesh",
    "Date" : "Sat Aug 16 2014 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "2",
    "People affected" : "",
    "Rescued" : ""
  },
  "21 Killed in Floods in Uttar Pradesh, India" :     {
    "Title" : "21 Killed in Floods in Uttar Pradesh, India",
    "Link" : "http://floodlist.com/asia/21-dead-floods-uttar-pradesh",
    "Location" : "Uttar Pradesh",
    "Date" : "Sun Aug 17 2014 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "21",
    "People affected" : "300000",
    "Rescued" : ""
  },
  "Floods in 9 Districts in Bihar, India" :     {
    "Title" : "Floods in 9 Districts in Bihar, India",
    "Link" : "http://floodlist.com/asia/floods-9-districts-bihar-india",
    "Location" : "Bihar",
    "Date" : "Sat Aug 16 2014 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "",
    "People affected" : "40000",
    "Rescued" : ""
  },
  "1 Dead, 2 Missing in Floods in Himachal Pradesh, Indi" :     {
    "Title" : "1 Dead, 2 Missing in Floods in Himachal Pradesh, Indi",
    "Link" : "http://floodlist.com/asia/floods-himachal-pradesh-india-august-2014",
    "Location" : "Hamirpur",
    "Date" : "Thu Aug 14 2014 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "3",
    "People affected" : "",
    "Rescued" : ""
  },
  "2 Dead in Uttarakhand, India" :     {
    "Title" : "2 Dead in Uttarakhand, India",
    "Link" : "http://floodlist.com/asia/2-dead-uttarakhand-india",
    "Location" : "Uttarakhand",
    "Date" : "Mon Jul 21 2014 03:00:00 GMT-0400 (EDT)",
    "Rainfall" : "",
    "Died" : "2",
    "People affected" : "",
    "Rescued" : ""
  },
  "Flooding and Hailstorms Hit Andhra Pradesh, India" :     {
    "Title" : "Flooding and Hailstorms Hit Andhra Pradesh, India",
    "Link" : "http://floodlist.com/asia/flooding-hailstorms-andhra-pradesh-india",
    "Location" : "Andhra Pradesh",
    "Date" : "Fri Mar 07 2014 03:00:00 GMT-0500 (EST)",
    "Rainfall" : "",
    "Died" : "",
    "People affected" : "58",
    "Rescued" : ""
  },
    "North East India Floods Update" :     {
    "Title" : "North East India Floods Update",
    "Link" : "http://floodlist.com/asia/north-east-india-floods-update",
    "Location" : "Odisha",
    "Date" : "Oct 18 2013",
    "Rainfall" : "",
    "Died" : "17",
    "People affected" : "",
    "Rescued" : ""
  },
    "North East India Floods Update" :     {
    "Title" : "North East India Floods Update",
    "Link" : "http://floodlist.com/asia/north-east-india-floods-update",
    "Location" : "Bhagalpur",
    "Date" : "Oct 16 2013",
    "Rainfall" : "232",
    "Died" : "",
    "People affected" : "",
    "Rescued" : ""
  },
    "North East India Floods Update" :     {
    "Title" : "North East India Floods Update",
    "Link" : "http://floodlist.com/asia/north-east-india-floods-update",
    "Location" : "Patna recorded",
    "Date" : "Oct 16 2013",
    "Rainfall" : "127",
    "Died" : "",
    "People affected" : "",
    "Rescued" : ""
  },
    "North East India Floods Update" :     {
    "Title" : "North East India Floods Update",
    "Link" : "http://floodlist.com/asia/north-east-india-floods-update",
    "Location" : "Gaya",
    "Date" : "Oct 16 2013",
    "Rainfall" : "112",
    "Died" : "",
    "People affected" : "",
    "Rescued" : ""
  },
    "North East India Floods Update" :     {
    "Title" : "North East India Floods Update",
    "Link" : "http://floodlist.com/asia/north-east-india-floods-update",
    "Location" : "Purnia",
    "Date" : "Oct 16 2013",
    "Rainfall" : "203",
    "Died" : "",
    "People affected" : "",
    "Rescued" : ""
  },
    "North East India Floods Update" :     {
    "Title" : "North East India Floods Update",
    "Link" : "http://floodlist.com/asia/north-east-india-floods-update",
    "Location" : "Vaishali",
    "Date" : "Oct 16 2013",
    "Rainfall" : "88",
    "Died" : "",
    "People affected" : "",
    "Rescued" : ""
  },
    "North East India Floods Update" :     {
    "Title" : "North East India Floods Update",
    "Link" : "http://floodlist.com/asia/north-east-india-floods-update",
    "Location" : "Muzaffarpur",
    "Date" : "Oct 16 2013",
    "Rainfall" : "122",
    "Died" : "",
    "People affected" : "",
    "Rescued" : ""
  },
   "Flooding in Gujarat, India" :     {
    "Title" : "Flooding in Gujarat, India",
    "Link" : "http://floodlist.com/asia/flooding-gujarat-india",
    "Location" : "Gujarat",
    "Date" : "Sep 27 2013",
    "Rainfall" : "122",
    "Died" : "13",
    "People affected" : "62000",
    "Rescued" : ""
  },
  "Flooding in Chennai, India" :     {
    "Title" : "Flooding in Chennai, India",
    "Link" : "http://foto.detik.com/readfoto/2015/12/03/102549/3086651/157/1/banjir-terjang-india?utm_content=www.jasafotosurabaya.com&utm_source=twitterfeed&utm_medium=facebook&utm_campaign=www.jasafotosurabaya.com",
    "Location" : "Chennai",
    "Date" : "Dec 03 2015",
    "Rainfall" : "",
    "Died" : "",
    "People affected" : "200000",
    "Rescued" : ""
  }
};