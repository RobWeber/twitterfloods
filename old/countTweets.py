import json

if __name__ == '__main__':
    
    cities = json.load(open("cities.json"));
    tweets = open("/media/rob/BACKUP_MEDIA/twitterFloods/india.json", "r");
    citiesDictionary = {}
    countLines = 1
       
    # loop through tweets
    for line in tweets:
        tweet_json = json.loads(line)["text"]
        if countLines % 10000 == 0:
            print countLines
            # convert to string to output to .csv
            output = 'city,lat,lng,count'
            for city in citiesDictionary:
                output += '\n' + citiesDictionary[city][0] + ',' + citiesDictionary[city][1] + ',' + citiesDictionary[city][2] + ',' + str(citiesDictionary[city][3])
            f = open("/media/rob/BACKUP_MEDIA/twitterFloods/allTweetsCounted.csv", "rw+")
            f.truncate()
            f.write(output)
            f.close()
        countLines = countLines + 1
        # loop through words in tweets
        for word in tweet_json.split(" "):
            # loop through cities
            for city in cities:
                # compare words in tweets with cities and add to dictionary
                if word == cities[city]['cityH']:
                    if citiesDictionary.get(cities[city]['city']) == None:
                        count = 1
                    else:
                        count = citiesDictionary.get(cities[city]['city'])[3] + 1
                    citiesDictionary[str(cities[city]['city'])] = [str(cities[city]['city']), str(cities[city]['lat']), str(cities[city]['lng']), count]
