<?php
  set_time_limit(0);
  /***
  // Original getTweets() parser for the india.json file
  function getTweets() {
    // Looks at the translated cities and at the text in the tweets.
    $citiesJson = file_get_contents("./cities.json");
    $citiesData = json_decode($citiesJson, TRUE);

    $tweets = file("india.json");

    $tweetText = array();    
    $x = 1;
    foreach($tweets as $line) {
        $tweetText[$x] = json_decode($line, TRUE);
        $x = $x + 1;
    }

    $returnArray = array();

    $count = 0;
    foreach ($tweetText as $key => $value) {
      foreach ($citiesData as $cities => $city) {
        if(strpos($value['text'], $city['cityH']) !== false) {
          $count = $count+1;
          $returnArray[$count]['city'] = $city['city'];
          $returnArray[$count]['lat'] = $city['lat'];
          $returnArray[$count]['lng'] = $city['lng'];
          $returnArray[$count]['date'] = gmdate("Y-m-d", strtotime($value['created_at']));
          $returnArray[$count]['tweets'] = 0;
          continue;
        }
      }
    }

    foreach ($returnArray as $key => $value) {
      foreach ($returnArray as $key2 => $value2) {
        if ($value['city'] == $value2['city']) {
          $returnArray[$key2]['tweets'] = $returnArray[$key2]['tweets'] + 1;
        }
      }
    }
    echo json_encode($returnArray);
  }**/


  function getAllTweetsWithDate() {
    $tweetsJson = file_get_contents("./tweetsSubset.json");
    echo $tweetsJson;
  }


  function getRiskAreas() {
    $polygon = array();
    $topLeft = null;
    $bottomLeft = null;
    $topRight = null;
    $bottomRight = null;
    $count = 0;

    $handle = fopen("riskAreas.json", "r");
    if ($handle) {
      while (($line = fgets($handle)) !== false) {
        $coordinates = explode(" ", $line);

        $topLeft = [trim($coordinates[0]), trim($coordinates[2])];
        $bottomLeft = [trim($coordinates[0]), trim($coordinates[3])];
        $topRight = [trim($coordinates[1]), trim($coordinates[2])];
        $bottomRight = [trim($coordinates[1]), trim($coordinates[3])];

        if ($topLeft == $bottomLeft || $topLeft == $topRight || $topLeft == $bottomRight ||
            $bottomLeft == $topLeft || $bottomLeft == $topRight || $bottomLeft == $bottomRight ||
            $topRight == $bottomLeft || $topRight == $topLeft || $topRight == $bottomRight ||
            $bottomRight == $bottomLeft || $bottomRight == $topRight || $bottomRight == $topLeft) {
          continue;
        }

        $polygon[$count]['topLeft'] = $topLeft;
        $polygon[$count]['bottomLeft'] = $bottomLeft;
        $polygon[$count]['topRight'] = $topRight;
        $polygon[$count]['bottomRight'] = $bottomRight;

        $count++;
      }
      fclose($handle);
    }

    echo json_encode($polygon);
  }

  
  // New getTweets for indiaNew.json file
  function getTweets() {
    // Looks at the translated cities and at the text in the tweets.
    $citiesJson = file_get_contents("./cities.json");
    $citiesData = json_decode($citiesJson, TRUE);

    $tweets = file("indiaNew.json");

    $tweetText = array();    
    $x = 1;
    foreach($tweets as $line) {
        $tweetText[$x] = json_decode($line, TRUE);
        $x = $x + 1;
    }

    $returnArray = array();
    $count = 0;
    foreach ($tweetText as $key => $value) {
      foreach ($citiesData as $cities => $city) {
        if(strpos($value['text'], $city['cityH']) !== false) {
          $count = $count+1;
          $returnArray[$count]['city'] = $city['city'];
          $returnArray[$count]['lat'] = $city['lat'];
          $returnArray[$count]['lng'] = $city['lng'];
          $returnArray[$count]['date'] = gmdate("Y-m-d", strtotime($value['date']['$date']));
          $returnArray[$count]['tweets'] = 0;
          continue;
        }
      }
    }

    foreach ($returnArray as $key => $value) {
      foreach ($returnArray as $key2 => $value2) {
        if ($value['city'] == $value2['city']) {
          $returnArray[$key2]['tweets'] = $returnArray[$key2]['tweets'] + 1;
        }
      }
    }
    echo json_encode($returnArray);
  }
  

  
  function getIndiaTweets() { 
    // Looks at the geo coordinates in the india.txt file.
    // Future improvement: Look at the actual tweet text and compare to translated cities list
    $contents = file('india.json');

    $tweetText = array();    
    $x = 1;
    foreach($contents as $line) {
        $tweetText[$x] = json_decode($line, TRUE);
        $x = $x + 1;
    }

    $returnArray = array();

    $count = 0;
    foreach ($tweetText as $key => $value) {
      $lat = $value['geo']['coordinates'][0];
      $lng = $value['geo']['coordinates'][1];
      $returnArray[$count]['text'] = $value['text'];
      $returnArray[$count]['lat'] = $lat;
      $returnArray[$count]['lng'] = $lng;
      $returnArray[$count]['date'] = substr($value['created_at'], 0, 11);
      if ($lat != null) {$count = $count + 1;}
    }
    echo json_encode($returnArray);
  }
  

  function getIndiaNews() {
    // Looks at the location in the news.json file and compares it with the cities.json
    // Future improvement: Only use the newest article if theres an article about the same place
    // Future improvement: Add more cities/regions to the cities.json file
    $newsJson = file_get_contents("./news.json");
    $newsData = json_decode($newsJson, TRUE);
    $citiesJson = file_get_contents("./cities.json");
    $citiesData = json_decode($citiesJson, TRUE);

    $newsLocations = array();    
    $x = 1;
    foreach ($newsData as $article) {
      $newsLocations[$x]["location"] = $article["Location"];
      $newsLocations[$x]["text"] = $article["Title"];
      $newsLocations[$x]["date"] = $article["Date"];
      $x = $x + 1;
    }
    
    $returnArray = array();

    foreach ($newsLocations as $news) {
      foreach ($citiesData as $cities => $city) {
        if(strpos($news["location"], $city["city"]) !== false) {
          $returnArray[$city['city']]['city'] = $city['city'];
          $returnArray[$city['city']]['lat'] = $city['lat'];
          $returnArray[$city['city']]['lng'] = $city['lng'];
          $returnArray[$city['city']]['text'] = $news['text'];
          //$returnArray[$city['city']]['date'] = substr($news['date'], 0, 16);
          $returnArray[$city['city']]['date'] = gmdate("m/d/Y", strtotime($news['date']));
          continue;
        }
      }
    }
    echo json_encode($returnArray);
  }


  function getHeight() {
    $heightJson = file_get_contents("./hoogte.json");
    echo $heightJson;
  }
?>


<html>
  <head>
    <title>TwitterFloods - FloodFlamingos</title>
    <script type="text/javascript" src="tweetsSubset.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&libraries=visualization"></script>
    <script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
    <script>

      var tweetCount = "lat,lng,tweetCount,city";
      var markerCluster; 
      var map;
      var geoJSON;
      var markers = [];
      var markersNews = [];
      var request;
      var gettingData = false;

      function downloadMarkerData(contents) {
        var textFile = null,
        makeTextFile = function (text) {
          var data = new Blob([text], {type: 'csv'});

          if (textFile !== null) {
            window.URL.revokeObjectURL(textFile);
          }

          textFile = window.URL.createObjectURL(data);

          return textFile;
        };

        var create = document.getElementById('create');

        create.addEventListener('click', function () {
          var link = document.getElementById('downloadlink');
          link.href = makeTextFile(contents);
          link.style.display = 'block';
        }, false);
      };

      function initialize() {
        var mapOptions = {
          zoom: 5,
          center: new google.maps.LatLng(21.199828, 77.615198),
          mapTypeId: google.maps.MapTypeId.TERRAIN
        };

        map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);

        // Add interaction listeners to make weather requests
        google.maps.event.addListener(map, 'idle', checkIfDataRequested);

        // Sets up and populates the info window with details
        map.data.addListener('click', function(event) {
          infowindow.setContent(
           "<img src=" + event.feature.getProperty("icon") + ">"
           + "<br /><strong>" + event.feature.getProperty("city") + "</strong>"
           + "<br />" + event.feature.getProperty("temperature") + "&deg;C"
           + "<br />" + event.feature.getProperty("weather")
           );
          infowindow.setOptions({
              position:{
                lat: event.latLng.lat(),
                lng: event.latLng.lng()
              },
              pixelOffset: {
                width: 0,
                height: -15
              }
            });
          infowindow.open(map);
        });


        var heightData = <?php getHeight(); ?>;
        var heatMapData = [];

        for (var x = 0; x < heightData.length; x++) {
          var obj = heightData[x];
          var lat = obj["lat"];
          var lng = obj["lng"];
          var height = obj["height"];
          var height = height / 17000;

          heatMapData.push({location: new google.maps.LatLng(lat, lng), weight: height});
        }

        heatmap = new google.maps.visualization.HeatmapLayer({
          data: heatMapData,
          map: null
        });
        
        setMarkers(map);
        setPolygons(map);
        //enable this for markerclusterer
        //markerCluster = new MarkerClusterer(map, markers, {maxZoom: 10});
      }

      function setPolygons(map) {
        var polygons = <?php getRiskAreas(); ?>;

        for (var i = 0; i < polygons.length; i++) {
          var topLeft = polygons[i]['topLeft'];
          var bottomLeft = polygons[i]['bottomLeft'];
          var topRight = polygons[i]['topRight'];
          var bottomRight = polygons[i]['bottomRight'];

          var polygonCoords = [
            {lat: parseInt(topLeft[0]), lng: parseInt(topLeft[1])},
            {lat: parseInt(topRight[0]), lng: parseInt(topRight[1])},
            {lat: parseInt(bottomRight[0]), lng: parseInt(bottomRight[1])},
            {lat: parseInt(bottomLeft[0]), lng: parseInt(bottomLeft[1])},
            {lat: parseInt(topLeft[0]), lng: parseInt(topLeft[1])}
          ];

          var riskArea = new google.maps.Polygon({
            paths: polygonCoords,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 0,
            fillColor: '#FF0000',
            fillOpacity: 0.35
          });
          riskArea.setMap(map);
        }

      }

      function toggleHeatmap() {
          heatmap.setMap(heatmap.getMap() ? null : map);
      }

      function changeGradient() {
        var gradient = [
          'rgba(0, 255, 255, 0)',
          'rgba(0, 255, 255, 1)',
          'rgba(0, 191, 255, 1)',
          'rgba(0, 127, 255, 1)',
          'rgba(0, 63, 255, 1)',
          'rgba(0, 0, 255, 1)',
          'rgba(0, 0, 223, 1)',
          'rgba(0, 0, 191, 1)',
          'rgba(0, 0, 159, 1)',
          'rgba(0, 0, 127, 1)',
          'rgba(63, 0, 91, 1)',
          'rgba(127, 0, 63, 1)',
          'rgba(191, 0, 31, 1)',
          'rgba(255, 0, 0, 1)'
        ]
        heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
      }

      function changeRadius() {
        heatmap.set('radius', heatmap.get('radius') ? null : 20);
      }

      function changeOpacity() {
        heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
      }



      var checkIfDataRequested = function() {
        // Stop extra requests being sent
        while (gettingData === true) {
          request.abort();
          gettingData = false;
        }
        getCoords();
      };


      // Get the coordinates from the Map bounds
      var getCoords = function() {
        var bounds = map.getBounds();
        var NE = bounds.getNorthEast();
        var SW = bounds.getSouthWest();
        getWeather(NE.lat(), NE.lng(), SW.lat(), SW.lng());
      };


      // Make the weather request
      var getWeather = function(northLat, eastLng, southLat, westLng) {
        gettingData = true;
        var requestString = "http://api.openweathermap.org/data/2.5/box/city?bbox="
                            + westLng + "," + northLat + "," //left top
                            + eastLng + "," + southLat + "," //right bottom
                            + map.getZoom()
                            + "&cluster=yes&format=json"
                            + "&APPID=" + "551b97ca557560dfc7d8c49a81b37d89";
        request = new XMLHttpRequest();
        request.onload = proccessResults;
        request.open("get", requestString, true);
        request.send();
      };


      // Take the JSON results and proccess them
      var proccessResults = function() {
        //console.log(this);
        var results = JSON.parse(this.responseText);
        if (results.list.length > 0) {
            resetData();
            for (var i = 0; i < results.list.length; i++) {
              geoJSON.features.push(jsonToGeoJson(results.list[i]));
            }
            drawIcons(geoJSON);
        }
      };

      var infowindow = new google.maps.InfoWindow();


      // For each result that comes back, convert the data to geoJSON
      var jsonToGeoJson = function (weatherItem) {
        var feature = {
          type: "Feature",
          properties: {
            city: weatherItem.name,
            weather: weatherItem.weather[0].main,
            temperature: weatherItem.main.temp,
            min: weatherItem.main.temp_min,
            max: weatherItem.main.temp_max,
            humidity: weatherItem.main.humidity,
            pressure: weatherItem.main.pressure,
            windSpeed: weatherItem.wind.speed,
            windDegrees: weatherItem.wind.deg,
            windGust: weatherItem.wind.gust,
            icon: "http://openweathermap.org/img/w/"
                  + weatherItem.weather[0].icon  + ".png",
            coordinates: [weatherItem.coord.lon, weatherItem.coord.lat]
          },
          geometry: {
            type: "Point",
            coordinates: [weatherItem.coord.lon, weatherItem.coord.lat]
          }
        };
        // Set the custom marker icon
        map.data.setStyle(function(feature) {
          return {
            icon: {
              url: feature.getProperty('icon'),
              anchor: new google.maps.Point(25, 25)
            }
          };
        });

        // returns object
        return feature;
      };


      // Add the markers to the map
      var drawIcons = function (weather) {
         map.data.addGeoJson(geoJSON);
         // Set the flag to finished
         gettingData = false;
      };


      // Clear data layer and geoJSON
      var resetData = function () {
        geoJSON = {
          type: "FeatureCollection",
          features: []
        };
        map.data.forEach(function(feature) {
          map.data.remove(feature);
        });
      };


      // Adds a marker to the map and push to the array.
      function addMarker(position, icon, city, count, text) {
        var markerImage = new google.maps.MarkerImage("https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=mobile|bb|" + count.toString() + "|4099FF|FFFFFF",
          new google.maps.Size(130, 50),
          new google.maps.Point(0, 0),
          new google.maps.Point(0, 50));
        var marker = new google.maps.Marker({
          position: position,
          icon: markerImage,
          title: city,
          map: map
        });

        var infowindow = new google.maps.InfoWindow()

        google.maps.event.addListener(marker,'click', (function(marker,text,infowindow){ 
          return function() {
            infowindow.setContent(text);
            infowindow.open(map,marker);
          };
        })(marker,text,infowindow));

        markers.push(marker);
      }

      function addMarkerNews(position, icon, city, count, text) {
        var markerImage = new google.maps.MarkerImage("https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=books|bbbr|" + count.toString() + "|F2F5A9|000000",
          new google.maps.Size(130, 50),
          new google.maps.Point(0, 0),
          new google.maps.Point(55, 50));
        var marker = new google.maps.Marker({
          position: position,
          icon: markerImage,
          title: city,
          map: map
        });

        var infowindow = new google.maps.InfoWindow()

        google.maps.event.addListener(marker,'click', (function(marker,text,infowindow){ 
          return function() {
            infowindow.setContent(text);
            infowindow.open(map,marker);
          };
        })(marker,text,infowindow));

        markersNews.push(marker);
      }


      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(null);
        }
        markers = [];

        for (var i = 0; i < markersNews.length; i++) {
          markersNews[i].setMap(null);
        }
        markersNews = [];
      }


      // Sets all the markers on the map (for tweets & news articles)
      function setMarkers(map, startDate, endDate) {
        deleteMarkers();

        if (!startDate && !endDate) {
          var startDate = new Date("01-01-1000");
          var endDate = new Date("01-01-3000");
        } else {
          var startDate = new Date(startDate);
          var endDate = new Date(endDate);
        }

        var markersPhp = <?php getTweets() ?>;
        var markersNewsPhp = <?php getIndiaNews() ?>;
        var markersTweets = <?php getAllTweetsWithDate() ?>;
        var latPrevious = "";

        /**
        // Add the tweets
        for (var i in markersTweets) {
          var lat = markersTweets[i]['lat'];
          var lng = markersTweets[i]['lng'];
          var city = markersTweets[i]['city'];
          var date =  new Date(markersTweets[i]['date']);

          if (startDate && endDate && (date > startDate) && (date < endDate)) {
            var latlngset = new google.maps.LatLng(lat, lng);
            var icon = "http://d236cvat6r51ek.cloudfront.net/assets/icon/twitter_icon-261b7a82e99260d33b412bc7497bd8f2.png";
            var text = "<b>Tweet</b><br>City: " + city;
            var count = 1;

            if (count == 1) {
              text = "<b>Tweet</b><br>City: " + city;
            } else {
              text = "<b>Tweet</b><br>Count: " + count;
            }

            addMarker(latlngset, icon, city, count, text);
          }
        }**/

        var count = 1;
        for (var index = 0; index < tweets.length; index++) {
          for (var index2 = 0; index2 < tweets.length; index2++) {
            if (tweets[index]['city'] == tweets[index2]['city']) {
              count++;
            }
          }
          var lat = tweets[index]['lat'];
          var lng = tweets[index]['lng'];
          var city = tweets[index]['city'];
          var date =  new Date(tweets[index]['date']);

          if (startDate && endDate && (date > startDate) && (date < endDate)) {
            var latlngset = new google.maps.LatLng(lat, lng);
            var icon = "http://d236cvat6r51ek.cloudfront.net/assets/icon/twitter_icon-261b7a82e99260d33b412bc7497bd8f2.png";
            var text = "<b>Tweet</b><br>City: " + city;
            //var count = 1;

            if (count == 1) {
              text = "<b>Tweet</b><br>City: " + city;
            } else {
              text = "<b>Tweet</b><br>City: " + city + "<br>Tweet count: " + count;
            }

            addMarker(latlngset, icon, city, count, text);
          }
          count = 1;
        }

        
        /** For the old tweets (from the huge tweet file)
        // Add the tweets
        for (var i in markersPhp) {
          var lat = markersPhp[i]['lat'];
          var lng = markersPhp[i]['lng'];
          var city = markersPhp[i]['city'];
          var count = markersPhp[i]['tweets'];
          var date =  new Date(markersPhp[i]['date']);

          if (lat != latPrevious) {
            tweetCount = tweetCount + "\n" + lat + "," + lng + "," + count + "," + city;
          }
          latPrevious = lat;

          if (startDate && endDate && (date > startDate) && (date < endDate)) {
            var latlngset = new google.maps.LatLng(lat, lng);
            var icon = "http://d236cvat6r51ek.cloudfront.net/assets/icon/twitter_icon-261b7a82e99260d33b412bc7497bd8f2.png";
            var text = "<b>Tweet</b><br>City: " + city + "<br>Count: " + count + " tweets";

            addMarker(latlngset, icon, city, count, text);
          }
        }**/

        // Add the news articles (with compares cities from the cities.json file)
        count = 0;
        for (var i in markersNewsPhp) {
          for (var r in markersNewsPhp) {
            if (markersNewsPhp[i]['city'] == markersNewsPhp[r]['city']) {
              count++;
            }
          }
          var city = markersNewsPhp[i]['city'];
          var lat = markersNewsPhp[i]['lat'];
          var long = markersNewsPhp[i]['lng'];
          var news =  markersNewsPhp[i]['text'];
          var date =  new Date(markersNewsPhp[i]['date']);

          if (startDate && endDate && (date > startDate) && (date < endDate)) {
            var latlngset = new google.maps.LatLng(lat, long);
            var icon = "http://www.ferroli.co.uk/wp-content/uploads/2012/07/news-icon.png";
            var dateString = date.toString();
            var text = "<b>News</b><br>" + city + ": " + news + ".<br>Date: " + dateString.substring(0, 15);
            //var count = 1;

            if (count == 1) {
              text = "<b>News</b><br>" + city + ": " + news + ".<br>Date: " + dateString.substring(0, 15);
            } else {
              text = "<b>News</b><br>City: " + city + "<br>Article count: " + count;
            }

            addMarkerNews(latlngset, icon, city, count, text);
          }
        }
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  </head>

  <body style="height:85%;">
    <h3>&nbsp;FloodFlamingos</h3>

    <div id="map-canvas" style="width:100%; height:85%"></div>

    <div>
      <br>&nbsp;&nbsp;&nbsp;<b>Show tweets and news from:</b>
      Start date: <input type="date" id="startDate" name="startDate">&nbsp;
      End date: <input type="date" id="endDate" name="endDate">&nbsp;
      <button onClick="setMarkers(map, startDate.value, endDate.value);">Show</button><br>
      <!--<br>&nbsp;&nbsp;&nbsp;<button onclick="toggleHeatmap()">Toggle Heatmap</button>
      <button onclick="changeGradient()">Change gradient</button>
      <button onclick="changeRadius()">Change radius</button>
      <button onclick="changeOpacity()">Change opacity</button><br><br>
      &nbsp;<a download="tweetCount.csv" id="downloadlink" style="display: none">Download generated tweetCount.csv file</a>
      &nbsp;<button onClick="downloadMarkerData(tweetCount);" id="create">Generate tweetCount data (for Mitchell) (doubleclick)</button>-->
    </div>

    

    <div>
      
    </div>
  </body>
</html>