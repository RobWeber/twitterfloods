<?php
	function getTweets() {
	    $citiesJson = file_get_contents("./cities.json");
	    $citiesData = json_decode($citiesJson, TRUE);

		$tweets = file("india.json");

		$tweetText = array();    
	    $x = 1;
		foreach($tweets as $line) {
		    $tweetText[$x] = json_decode($line, TRUE);
		    $x = $x + 1;
		}

		$returnArray = array();

		$count = 0;
		foreach ($tweetText as $key => $value) {
			foreach ($citiesData as $cities => $city) {
				if(strpos($value['text'], $city['cityH']) !== false) {
					$count = $count+1;
					$returnArray[$count]['city'] = $city['city'];
					$returnArray[$count]['lat'] = $city['lat'];
					$returnArray[$count]['lng'] = $city['lng'];
					$returnArray[$count]['tweets'] = 0;
					continue;
				}
			}
		}

		foreach ($returnArray as $key => $value) {
			foreach ($returnArray as $key2 => $value2) {
				if ($value['city'] == $value2['city']) {
					$returnArray[$key2]['tweets'] = $returnArray[$key2]['tweets'] + 1;
				}
			}
	    }

		echo json_encode($returnArray);
	}


	function getIndiaTweets() {
		$filename = 'india.txt';
		$contents = file($filename);

		$tweetText = array();    
	    $x = 1;
		foreach($contents as $line) {
		    $tweetText[$x] = json_decode($line, TRUE);
		    $x = $x + 1;
		}

		$returnArray = array();

		$count = 0;
		foreach ($tweetText as $key => $value) {
			$lat = $value['geo']['coordinates'][0];
			$lng = $value['geo']['coordinates'][1];
			$returnArray[$count]['text'] = $value['text'];
			$returnArray[$count]['lat'] = $lat;
			$returnArray[$count]['lng'] = $lng;
			$returnArray[$count]['date'] = substr($value['created_at'], 0, 11);
			if ($lat != null) {$count = $count + 1;}
		}

		echo json_encode($returnArray);
	}


	function getIndiaNews() {
		$newsJson = file_get_contents("./news.json");
	    $newsData = json_decode($newsJson, TRUE);
	    $citiesJson = file_get_contents("./cities.json");
	    $citiesData = json_decode($citiesJson, TRUE);

	    $newsLocations = array();    
	    $x = 1;
	    foreach ($newsData as $article) {
	    	$newsLocations[$x]["location"] = $article["Location"];
	    	$newsLocations[$x]["text"] = $article["Title"];
	    	$newsLocations[$x]["date"] = $article["Date"];
	    	$x = $x + 1;
	    }
	    
	    $returnArray = array();

	    foreach ($newsLocations as $news) {
	    	foreach ($citiesData as $cities => $city) {
	    		if(strpos($news["location"], $city["city"]) !== false) {
	    			$returnArray[$city['city']]['city'] = $city['city'];
					$returnArray[$city['city']]['lat'] = $city['lat'];
					$returnArray[$city['city']]['lng'] = $city['lng'];
					$returnArray[$city['city']]['text'] = $news['text'];
					$returnArray[$city['city']]['date'] = substr($news['date'], 0, 16);
					continue;
				}
	    	}
	    }
		echo json_encode($returnArray);
	}
?>

<html>
	<head>
		<title>TwitterFloods - FloodFlamingos</title>

		<script src="http://maps.googleapis.com/maps/api/js?v=3&sensor=false"></script>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

		<script>
			function initialize() {

				var myOptions = {
					center: new google.maps.LatLng(21.199828, 77.615198),
					zoom: 5,
					mapTypeId: google.maps.MapTypeId.TERRAIN
				};
				var map = new google.maps.Map(document.getElementById("default"),
				myOptions);

				setMarkers(map)
			}

			function setMarkers(map) {
				var markersPhp = <?php getIndiaTweets() ?>;
				var markersNewsPhp = <?php getIndiaNews() ?>;

				// Add the tweets
				for (var i in markersPhp) {

					var text = markersPhp[i]['text'];
					var lat = markersPhp[i]['lat'];
					var long = markersPhp[i]['lng'];
					var date =  markersPhp[i]['date'];

					latlngset = new google.maps.LatLng(lat, long);

					var marker = new google.maps.Marker({  
						map: map, title: city , position: latlngset,
						icon: "http://d236cvat6r51ek.cloudfront.net/assets/icon/twitter_icon-261b7a82e99260d33b412bc7497bd8f2.png"  
					});

					var content = "<b>Tweet</b><br>Text: " + text + ".<br>Date: " + date; // for getIndiaTweets() function

					var infowindow = new google.maps.InfoWindow()

					google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
						return function() {
							infowindow.setContent(content);
							infowindow.open(map,marker);
						};
					})(marker,content,infowindow));

				}

				// Add the news articles
				for (var i in markersNewsPhp) {
					var city = markersNewsPhp[i]['city'];
					var lat = markersNewsPhp[i]['lat'];
					var long = markersNewsPhp[i]['lng'];
					var text =  markersNewsPhp[i]['text'];
					var date =  markersNewsPhp[i]['date'];

					latlngset = new google.maps.LatLng(lat, long);

					var marker = new google.maps.Marker({  
						map: map, title: city , position: latlngset,
						icon: "http://www.ferroli.co.uk/wp-content/uploads/2012/07/news-icon.png"  
					});
					
					var content = "<b>News</b><br>" + city + ": " + text + ".<br>Date: " + date; // for getIndiaTweets() function

					var infowindow = new google.maps.InfoWindow()

					google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
						return function() {
							infowindow.setContent(content);
							infowindow.open(map,marker);
						};
					})(marker,content,infowindow));

				}
			}
		</script>
	</head>
<img src="flamingo2.jpg", style="width:180px;height:120px;">
	<body onload="initialize()">
		<h3>&nbsp;FloodFlamingos (<a href="index.php">Turn weather data on</a>)</h3><br>
		<div id="default" style="width:100%; height:85%"></div>
	</body>

</html>