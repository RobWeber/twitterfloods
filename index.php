<?php
  set_time_limit(0);


  // Gets the data from the "risk" areas (based on hight) and processes data
  function getRiskAreas() {
    $polygon = array();
    $topLeft = null;
    $bottomLeft = null;
    $topRight = null;
    $bottomRight = null;
    $count = 0;

    $handle = fopen("riskAreas.json", "r");
    if ($handle) {
      while (($line = fgets($handle)) !== false) {
        $coordinates = explode(" ", $line);

        $topLeft = [trim($coordinates[0]), trim($coordinates[2])];
        $bottomLeft = [trim($coordinates[0]), trim($coordinates[3])];
        $topRight = [trim($coordinates[1]), trim($coordinates[2])];
        $bottomRight = [trim($coordinates[1]), trim($coordinates[3])];

        if ($topLeft == $bottomLeft || $topLeft == $topRight || $topLeft == $bottomRight ||
            $bottomLeft == $topLeft || $bottomLeft == $topRight || $bottomLeft == $bottomRight ||
            $topRight == $bottomLeft || $topRight == $topLeft || $topRight == $bottomRight ||
            $bottomRight == $bottomLeft || $bottomRight == $topRight || $bottomRight == $topLeft) {
          continue;
        }

        $polygon[$count]['topLeft'] = $topLeft;
        $polygon[$count]['bottomLeft'] = $bottomLeft;
        $polygon[$count]['topRight'] = $topRight;
        $polygon[$count]['bottomRight'] = $bottomRight;

        $count++;
      }
      fclose($handle);
    }
    echo json_encode($polygon);
  }
  

  // Looks at the location in the news.json file and compares it with the cities.json
  function getIndiaNews() {
    $newsJson = file_get_contents("./news.json");
    $newsData = json_decode($newsJson, TRUE);
    $citiesJson = file_get_contents("./cities.json");
    $citiesData = json_decode($citiesJson, TRUE);

    $newsLocations = array();    
    $count = 1;
    foreach ($newsData as $article) {
      $newsLocations[$count]["location"] = $article["Location"];
      $newsLocations[$count]["text"] = $article["Title"];
      $newsLocations[$count]["date"] = $article["Date"];
      $count = $count + 1;
    }
    
    $returnArray = array();

    $count = 0;
    foreach ($newsLocations as $news) {
      foreach ($citiesData as $cities => $city) {
        if(strpos($news["location"], $city["city"]) !== false) {
          $returnArray[$count]['city'] = $city['city'];
          $returnArray[$count]['lat'] = $city['lat'];
          $returnArray[$count]['lng'] = $city['lng'];
          $returnArray[$count]['text'] = $news['text'];
          $returnArray[$count]['date'] = gmdate("m/d/Y", strtotime($news['date']));
          $count = $count + 1;
        }
      }
    }
    echo json_encode($returnArray);
  }
?>


<html>
  <head>
    <title>TwitterFloods - FloodFlamingos</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script type="text/javascript" src="tweetsSubset.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&libraries=visualization"></script>
    
    <script>
      var map;
      var geoJSON;
      var markers = [];
      var markersNews = [];
      var request;
      var gettingData = false;
      var count = 0;


      function initialize() {
        var mapOptions = {
          zoom: 5,
          center: new google.maps.LatLng(21.199828, 77.615198),
          mapTypeId: google.maps.MapTypeId.TERRAIN
        };

        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        // For the Weather data
        google.maps.event.addListener(map, 'idle', checkIfDataRequested);
        map.data.addListener('click', function(event) {
          infowindow.setContent(
           "<img src=" + event.feature.getProperty("icon") + ">"
           + "<br /><strong>" + event.feature.getProperty("city") + "</strong>"
           + "<br />" + event.feature.getProperty("temperature") + "&deg;C"
           + "<br />" + event.feature.getProperty("weather")
           );
          infowindow.setOptions({
              position:{
                lat: event.latLng.lat(),
                lng: event.latLng.lng()
              },
              pixelOffset: {
                width: 0,
                height: -15
              }
            });
          infowindow.open(map);
        });
        
        setMarkers(map);
        setPolygons(map);
      }


      function setPolygons(map) {
        var polygons = <?php getRiskAreas(); ?>;

        for (var i = 0; i < polygons.length; i++) {
          var topLeft = polygons[i]['topLeft'];
          var bottomLeft = polygons[i]['bottomLeft'];
          var topRight = polygons[i]['topRight'];
          var bottomRight = polygons[i]['bottomRight'];

          var polygonCoords = [
            {lat: parseInt(topLeft[0]), lng: parseInt(topLeft[1])},
            {lat: parseInt(topRight[0]), lng: parseInt(topRight[1])},
            {lat: parseInt(bottomRight[0]), lng: parseInt(bottomRight[1])},
            {lat: parseInt(bottomLeft[0]), lng: parseInt(bottomLeft[1])},
            {lat: parseInt(topLeft[0]), lng: parseInt(topLeft[1])}
          ];

          var riskArea = new google.maps.Polygon({
            paths: polygonCoords,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 0,
            fillColor: '#FF0000',
            fillOpacity: 0.25
          });
          riskArea.setMap(map);
        }

      }


      // All the below code is for the Weather data plugin
      var checkIfDataRequested = function() {
        // Stop extra requests being sent
        while (gettingData === true) {
          request.abort();
          gettingData = false;
        }
        getCoords();
      };
      // Get the coordinates from the Map bounds
      var getCoords = function() {
        var bounds = map.getBounds();
        var NE = bounds.getNorthEast();
        var SW = bounds.getSouthWest();
        getWeather(NE.lat(), NE.lng(), SW.lat(), SW.lng());
      };
      // Make the weather request
      var getWeather = function(northLat, eastLng, southLat, westLng) {
        gettingData = true;
        var requestString = "http://api.openweathermap.org/data/2.5/box/city?bbox="
                            + westLng + "," + northLat + "," //left top
                            + eastLng + "," + southLat + "," //right bottom
                            + map.getZoom()
                            + "&cluster=yes&format=json"
                            + "&APPID=" + "551b97ca557560dfc7d8c49a81b37d89";
        request = new XMLHttpRequest();
        request.onload = proccessResults;
        request.open("get", requestString, true);
        request.send();
      };
      // Take the JSON results and proccess them
      var proccessResults = function() {
        //console.log(this);
        var results = JSON.parse(this.responseText);
        if (results.list.length > 0) {
            resetData();
            for (var i = 0; i < results.list.length; i++) {
              geoJSON.features.push(jsonToGeoJson(results.list[i]));
            }
            drawIcons(geoJSON);
        }
      };
      var infowindow = new google.maps.InfoWindow();
      // For each result that comes back, convert the data to geoJSON
      var jsonToGeoJson = function (weatherItem) {
        var feature = {
          type: "Feature",
          properties: {
            city: weatherItem.name,
            weather: weatherItem.weather[0].main,
            temperature: weatherItem.main.temp,
            min: weatherItem.main.temp_min,
            max: weatherItem.main.temp_max,
            humidity: weatherItem.main.humidity,
            pressure: weatherItem.main.pressure,
            windSpeed: weatherItem.wind.speed,
            windDegrees: weatherItem.wind.deg,
            windGust: weatherItem.wind.gust,
            icon: "http://openweathermap.org/img/w/"
                  + weatherItem.weather[0].icon  + ".png",
            coordinates: [weatherItem.coord.lon, weatherItem.coord.lat]
          },
          geometry: {
            type: "Point",
            coordinates: [weatherItem.coord.lon, weatherItem.coord.lat]
          }
        };
        // Set the custom marker icon
        map.data.setStyle(function(feature) {
          return {
            icon: {
              url: feature.getProperty('icon'),
              anchor: new google.maps.Point(25, 25)
            }
          };
        });
        // returns object
        return feature;
      };
      // Add the markers to the map
      var drawIcons = function (weather) {
         map.data.addGeoJson(geoJSON);
         // Set the flag to finished
         gettingData = false;
      };
      // Clear data layer and geoJSON
      var resetData = function () {
        geoJSON = {
          type: "FeatureCollection",
          features: []
        };
        map.data.forEach(function(feature) {
          map.data.remove(feature);
        });
      };


      // Adds a marker to the map and push to the array.
      function addMarker(position, city, count, text, tweetOrNews) {
        var markerImage = new google.maps.MarkerImage("https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=mobile|bb|" + count.toString() + "|4099FF|FFFFFF",
          new google.maps.Size(130, 50),
          new google.maps.Point(0, 0),
          new google.maps.Point(0, 50));
        var marker = new google.maps.Marker({
          position: position,
          icon: markerImage,
          title: city,
          map: map
        });

        var infowindow = new google.maps.InfoWindow()

        google.maps.event.addListener(marker,'click', (function(marker,text,infowindow){ 
          return function() {
            infowindow.setContent(text);
            infowindow.open(map,marker);
          };
        })(marker,text,infowindow));

        markers.push(marker);
      }


      function addMarkerNews(position, city, count, text, tweetOrNews) {
        var markerImage = new google.maps.MarkerImage("https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=books|bbbr|" + count.toString() + "|F2F5A9|000000",
          new google.maps.Size(130, 50),
          new google.maps.Point(0, 0),
          new google.maps.Point(55, 50));
        var marker = new google.maps.Marker({
          position: position,
          icon: markerImage,
          title: city,
          map: map
        });

        var infowindow = new google.maps.InfoWindow()

        google.maps.event.addListener(marker,'click', (function(marker,text,infowindow){ 
          return function() {
            infowindow.setContent(text);
            infowindow.open(map,marker);
          };
        })(marker,text,infowindow));

        markersNews.push(marker);
      }


      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(null);
        }
        markers = [];
        count = 0;

        for (var i = 0; i < markersNews.length; i++) {
          markersNews[i].setMap(null);
        }
        markersNews = [];
        count = 0;
      }


      // Sets all the markers on the map (for tweets & news articles)
      function setMarkers(map, startDate, endDate) {
        deleteMarkers();

        if (!startDate && !endDate) {
          var startDate = new Date("01-01-1000");
          var endDate = new Date("01-01-3000");
        } else {
          var startDate = new Date(startDate);
          var endDate = new Date(endDate);
        }

        var markersNewsPhp = <?php getIndiaNews() ?>;


        // Add the tweets
        count = 0;
        var mostRecent = new Date("01-01-1000");
        for (var index = 0; index < tweets.length; index++) {
          for (var index2 = 0; index2 < tweets.length; index2++) {
            if (tweets[index]['city'] == tweets[index2]['city']) {
              var date = new Date(tweets[index]['date']);
              var date2 = new Date(tweets[index2]['date']);
              if (startDate && endDate && (date > startDate) && (date < endDate) && (date2 > startDate) && (date2 < endDate)) { 
                if (date >= date2 && date >= mostRecent) {
                  mostRecent = date;
                } else if (date2 >= date && date2 >= mostRecent) {
                  mostRecent = date2;
                }
                count++;
              }
            }
          }
          var lat = tweets[index]['lat'];
          var lng = tweets[index]['lng'];
          var city = tweets[index]['city'];
          var date =  new Date(tweets[index]['date']);

          if (startDate && endDate && (date > startDate) && (date < endDate)) {
            var latlngset = new google.maps.LatLng(lat, lng);
            var text = "<b>Tweet</b><br>City: " + city + "<br>Tweet count: " + count + "<br>Most recent tweet: " + mostRecent.toString().substring(0, 15);

            addMarker(latlngset, city, count, text);
          }
          count = 0;
          mostRecent = new Date("01-01-1000");
        }


        // Add the news articles
        for (var index = 0; index < markersNewsPhp.length; index++) {
          for (var index2 = 0; index2 < markersNewsPhp.length; index2++) {
            if (markersNewsPhp[index]['city'] == markersNewsPhp[index2]['city']) {
              var date = new Date(markersNewsPhp[index]['date']);
              var date2 = new Date(markersNewsPhp[index2]['date']);
              if (startDate && endDate && (date > startDate) && (date < endDate) && (date2 > startDate) && (date2 < endDate)) {
                if (date >= date2 && date >= mostRecent) {
                  mostRecent = date;
                } else if (date2 >= date && date2 >= mostRecent) {
                  mostRecent = date2;
                }
                count++;
              }
            }
          }
          var city = markersNewsPhp[index]['city'];
          var lat = markersNewsPhp[index]['lat'];
          var long = markersNewsPhp[index]['lng'];
          var news =  markersNewsPhp[index]['text'];
          var date =  new Date(markersNewsPhp[index]['date']);

          if (startDate && endDate && (date > startDate) && (date < endDate)) {
            var latlngset = new google.maps.LatLng(lat, long);
            var dateString = date.toString();
            var text =  "<b>News article</b><br>City: " + city + "<br>Article count: " + count +
                        "<br>Most recent article date: " + mostRecent.toString().substring(0, 15) +
                        "<br>Most recent article: " + news;

            addMarkerNews(latlngset, city, count, text);
          }
          count = 0;
          mostRecent = new Date("01-01-1000");
        }
      }

      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  </head>

  <body style="height:95%;">
    <div>
      <h3>&nbsp;FloodFlamingos</h3>
      &nbsp;&nbsp;Start date: <input type="date" id="startDate" name="startDate">&nbsp;&nbsp;&nbsp;
      End date: <input type="date" id="endDate" name="endDate">&nbsp;&nbsp;&nbsp;
      <button onClick="setMarkers(map, startDate.value, endDate.value);">Show</button><br><br>
    </div>

    <div id="map-canvas" style="width:100%; height:85%"></div>

  </body>
</html>