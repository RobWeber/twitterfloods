<?php
  function getLocations() {
    // Looks at the translated cities and at the text in the tweets.
    $logData = json_decode(file_get_contents("./log.json"), TRUE);

    $returnArray = array();
    $count = 0;

    foreach ($logData as $key => $value) {
      $lat = $value['country']['lat'];
      $lng = $value['country']['long'];
      
      if ($lat && $lng) {
        if ($lat != $latPrev && $lng != $lngPrev) {
          $returnArray[$count]['lat'] = $lat;
          $returnArray[$count]['lng'] = $lng;
          $count++;
        }
      }      
    }

    echo json_encode($returnArray);
  }
?>

<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Security</title>
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
  </head>
  <body>
    <div id="map"></div>
    <script>
      var markers = [];
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 2,
          center: {lat: 0, lng: 0}
        });

        setMarkers(map);
        markerCluster = new MarkerClusterer(map, markers, {maxZoom: 10}); 
      }

      function setMarkers(map) {
        var markersPhp = <?php getLocations() ?>;

        for (var i in markersPhp) {
          var lat = markersPhp[i]['lat'];
          var lng = markersPhp[i]['lng'];

          var latlngset = new google.maps.LatLng(lat, lng);
          var latString = latlngset.toString();
        
          var marker = new google.maps.Marker({
            position: latlngset,
            map: map,
            title: latString
          });

          markers.push(marker);
        }
      }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?v=3&callback=initMap"></script>
    <script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer.js"></script>
  </body>
</html>