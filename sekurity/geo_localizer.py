import maxminddb
import json
import csv
import os
import glob


reader = maxminddb.open_database('E:\Documents\Data Science\Sekurity\GeoLite2-Country.mmdb')

os.chdir("E:\Desktop\logs")
dicto = {}

def buildLocationDict():
	csv_file = open("E:\Documents\Data Science\Sekurity\country_centroids_all.csv")
	csv_dict = csv.DictReader(csv_file)

	for country in csv_dict:
		code = country[" ISO3136"]
		if (code is None) :
			code = country[" FIPS10"]

		code.strip()

		dicto[code] = {}
		dicto[code]["iso_code"] = code
		dicto[code]["long"] = country[" LONG"].strip()
		dicto[code]["lat"] = country["LAT"].strip()

	print("Finished building Location dictionary")
	csv_file.close()


def locate(result):
	location = {}
	location["iso_code"] = "Unknown"
	location["long"] = -36
	location["lat"] = 14

	try:
		location = dicto[result["country"]["iso_code"].strip()]
	except (KeyError, TypeError):
		pass

	return location;



def processLogs():
	for file_name in glob.glob("*.json"):
		print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
		print("@\t\t" + file_name + "\t\t@")
		print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
		with open(file_name) as data_file:    
			data = json.load(data_file)

			for action in data:
				if (action["eventid"] == "KIPP0001" and action["src_ip"] is not None):
					print(action["src_ip"])

					result = reader.get(action["src_ip"])
					action["country"] = locate(result)

					jsonFile = open(file_name, "w+")
					jsonFile.write(json.dumps(data, sort_keys=True, indent=4))
					jsonFile.close()




def main():
	buildLocationDict()
	processLogs()

main()

print("Closing reader")
reader.close()