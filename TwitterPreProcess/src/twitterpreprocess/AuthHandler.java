/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twitterpreprocess;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import sun.rmi.runtime.Log;

/**
 *
 * @author lars
 */
public class AuthHandler {
    public AuthHandler() {

    }

    public JSONObject requestTokens() {
        HttpURLConnection conn = null;

        try {
            URL url = new URL("https://datamarket.accesscontrol.windows.net/v2/OAuth2-13");

            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");

            conn.setUseCaches(false);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));

            HashMap<String, String> params = new HashMap<>();
            params.put("client_id", "floodflamingostranslator");
            params.put("client_secret", "Cb71scgaUm4lVxyVAGwYSNfVBBA5RzfGA/3isQ/XEVU=");
            params.put("scope", "http://api.microsofttranslator.com");
            params.put("grant_type", "client_credentials");

            writer.write(getPostDataString(params));

            writer.flush();
            writer.close();
            os.close();

            //Get Response
            InputStream is = conn.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if not Java 5+
            String line;
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            
            JSONObject jsonObj;
            JSONParser parser = new JSONParser();

            jsonObj = (JSONObject) parser.parse(response.toString());
            
            return jsonObj;
        } catch (IOException e) {
        } catch (ParseException ex) {
            Logger.getLogger(AuthHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new JSONObject();
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException{
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }
}
