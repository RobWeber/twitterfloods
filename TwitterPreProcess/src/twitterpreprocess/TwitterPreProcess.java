/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twitterpreprocess;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author lars
 */
public class TwitterPreProcess {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        FileWriter file = null;
        try {
            // TODO code application logic here
            JSONObject tokens = new AuthHandler().requestTokens();
            
            JSONArray tweets = new TweetParser().parseTweets();
            Translator tweetTranslator = new Translator();
            
            int index = 0;
            int total = tweets.size();
            
            for (Object tweet1 : tweets) {
                JSONObject tweet = (JSONObject) tweet1;
                String text = (String) tweet.get("text");
                String translation = tweetTranslator.translate(tokens, text);
                tweet.put("text_translation", translation);
                
                index++;
                System.out.println("Finished translating [" + index + "/" + total + "]");
            }   
            
            System.out.println("Saving translations to file");
            
            file = new FileWriter("E:\\Documents\\Data Science\\FloodFlamingos\\india\\translation.json");
            file.write(tweets.toJSONString());
            file.flush();
            file.close();
            
        } catch (IOException ex) {
            Logger.getLogger(TwitterPreProcess.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                file.close();
            } catch (IOException ex) {
                Logger.getLogger(TwitterPreProcess.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        
    }
    
}
