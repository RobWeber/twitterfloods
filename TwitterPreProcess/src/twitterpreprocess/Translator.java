/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twitterpreprocess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author lars
 */
public class Translator {
    public Translator () {}
    
    public String translate(JSONObject tokens, String textToTranslate) {
        HttpURLConnection conn = null;
        
        HashMap<String, String> params = new HashMap<>();
        params.put("from", "hi");
        params.put("to", "en");
        params.put("text", textToTranslate);

        String queryString = buildQueryParams(params);
                
        try {
            URL url = new URL("http://api.microsofttranslator.com/V2/Http.svc/Translate?" + queryString);
            
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            
            conn.setRequestProperty ("Authorization", "Bearer " + tokens.get("access_token"));
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            conn.setUseCaches(false);
            conn.setDoOutput(true);

            //Get Response
            InputStream is = conn.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if not Java 5+
            String line;
            
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();

            String xml = response.toString();
            xml = xml.replace("<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">", "");
            xml = xml.replace("</string>", "");
            
            return xml;
        } catch (MalformedURLException ex) {
            Logger.getLogger(Translator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ProtocolException ex) {
            Logger.getLogger(Translator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Translator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }


    private String buildQueryParams(Map<String, String> params) {
        StringBuilder requestParams = new StringBuilder();
        // creates the params string, encode them using URLEncoder
        Iterator<String> paramIterator = params.keySet().iterator();
        while (paramIterator.hasNext()) {
            try {
                String key = paramIterator.next();
                String value = params.get(key);
                requestParams.append(URLEncoder.encode(key, "UTF-8"));
                requestParams.append("=").append(
                        URLEncoder.encode(value, "UTF-8"));
                requestParams.append("&");
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(Translator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
       
        String queryString = requestParams.toString();
        return queryString;
    }
}
